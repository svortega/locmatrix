#!/usr/bin/env python

"""Setup script for packaging fem2ufo.

Requires setuptools.

To build the setuptools egg use
    python setup.py bdist_egg
and either upload it to the PyPI with:
    python setup.py upload
or upload to your own server and register the release with PyPI:
    python setup.py register

A source distribution (.zip) can be built with
    python setup.py sdist --format=zip

That uses the manifest.in file for data files rather than searching for
them here.

"""

import os
import sys
import warnings

if sys.version_info < (3, 6):
    raise Exception("Python >= 3.6 is required.")

from setuptools import setup, Extension, find_packages
import re

here = os.path.abspath(os.path.dirname(__file__))
try:
    with open(os.path.join(here, 'README.rst')) as f:
        README = f.read()
except IOError:
    README = ''

__author__ = 'See AUTHORS'
__license__ = 'MIT/Expat'
__author_email__ = 'svortega@gmail.com'
__maintainer_email__ = 'svortega@gmail.com'
# __url__ = 

# import fem2ufo  # to fetch __version__ etc
def get_version():
    f = open(os.path.join(here, 'locmatrix', '__init__.py'))
    version_file = f.read()
    f.close()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(name='locmatrix',
      packages=find_packages(),
      # metadata
      version=get_version(),
      description="A Python library to create load combinations",
      long_description=README,
      author=__author__,
      author_email=__author_email__,
      # url = __url__,
      license=__license__,
      requires=[
          'python (>=3.5.0)',
          'xlwings (>= 0.10)'],
      classifiers=['Development Status :: 2 - Alpha',
                   'Operating System :: MacOS :: MacOS X',
                   'Operating System :: Microsoft :: Windows',
                   'License :: OSI Approved :: MIT License',
                   'Programming Language :: Python :: 3'],
      )

# Copyright (c) 2020 LoCmatrix
#
# Python stdlib imports
#
# package imports
import locmatrix as lcm


def run_macro():
    lcm.get_combinations()
#
def run_soil_checks():
    lcm.soil_check()
    
#
# This is for debugging purposes
#
if __name__ == '__main__':
    import os
    import xlwings as xw
    # Expects the Excel file next to this source file, adjust accordingly.
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'LoCmatrix_v027.xlsm'))
    #path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'LoCmatrix_v027_Shanice.xlsm'))
    xw.Book(path).set_mock_caller()
    lcm.get_combinations()
    #lcm.soil_check()
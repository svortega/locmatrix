![LoCmatrix_small.jpg](https://bitbucket.org/repo/8GbGkB/images/4149106658-LoCmatrix_small.jpg)
# **LoCmatrix**  
-------------  
## LoCmatrix is an excel spreadsheet to create load combinations for offshore structures analysis. 
## Load combinations can be exported to the following formats:  
* SESAM-GeniE  
* SACS  
* USFOS  

##The spreadsheet requires [Python +3.6](https://www.python.org/) and [xlwings +V0.18](https://www.xlwings.org/). The easiest way is to install [Anaconda](https://www.continuum.io/downloads).  

##Version: V0.2.5.1##
## Date: April 2020 ##

##Licence : [MIT](https://bitbucket.org/svortega/locmatrix/src/a3010981cb7becf523a660aecf441d85bebd897b/LICENCE.rst?at=V0.20&fileviewer=file-view-default)

# QuickStart #
--------------
## Installation ##
* Download LoCmatrix from [repository](https://bitbucket.org/svortega/locmatrix/downloads/) and unzipped it 
* Within the folder you will find an example of the spreadsheet.

## Spreadsheet Sheet Input Data ##
### Master 
In the main menu use to select the input data sheets with the relevant information and the selection of output format.

### Foundation  
The foundation sheet comprises the information of the soil data:

1. Soil layers section
2. Nonlinear spring (Py, Tz & Qz) 

### Metocean  
The Metocean sheet comprises the seastate data: 

1. Seastate combination section
2. Wave 
3. Current 
4. Wind 
5. Wind Areas

### First Level Load Combination
The First Level load combination sheet combines the basic load cases and the seastate load cases (from the Seastate sheet) with adjustment factors. The First Level defines API-WSD, ISO P50 or USFOS load combinations.

### Second Level Load Combination  
The Second Level load combination sheet combines the First Level load combinations with design code load factors. The Second Level defines API-LRFD or ISO load combinations.
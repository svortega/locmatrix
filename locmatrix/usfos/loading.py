# 
# Copyright (c) 2009-2017 fem2ufo
# 


#
# Python stdlib imports

# package imports
import locmatrix.process.printing as printing
#import locmatrix.process.operations as operations


# --------------------------------------------------------------------
# Loading control
#
#
def print_basic_load_combination(level_1, functional, lc_no):
    """
    """
    #
    FEMufo = []
    FEMufo.append("'    This record defines USFOS load combinations \n")
    FEMufo.append("'\n")
    FEMufo.append("' L1 Combination Name   : {:}\n".format(level_1.name))
    FEMufo.append("'                Number : {:}\n".format(level_1.number))
    FEMufo.append("'\n")
    
    _space = 24*" "
    if lc_no == 3 :
        FEMufo.append("'          Comb_Case   L_Case[i]   L1_Factor[n]\n")
        FEMufo.append("COMBLOAD     {:7.0f}".format(lc_no))
        _space = "    "
    
    for comb in level_1.functional_load:
        basic_load = functional[comb[0]]
        FEMufo.append("{:} {:7.0f}  {: 1.6e}  ! {:}\n"
                      .format(_space, basic_load.number, 
                              comb[1], basic_load.name))
        _space = 24*" "
    #
    FEMufo.append("'\n")

    return FEMufo
#
def print_combination(factor, level_1, functional, lc_no):
    """
    """
    #
    FEMufo = []
    FEMufo.append("'\n")
    FEMufo.append("' L1 Combination Name   : {:}\n".format(level_1.name))
    FEMufo.append("'                Number : {:}\n".format(level_1.number))
    FEMufo.append("'\n")
    
    _space = 24*" "
    if lc_no == 3 :
        FEMufo.append("'          Comb_Case   L_Case[i]   L1_Factor[n]\n")
        FEMufo.append("COMBLOAD     {:7.0f}".format(lc_no))
        _space = "    "
    
    for comb in level_1.functional_load:
        basic_load = functional[comb[0]]
        FEMufo.append("{:} {:7.0f}  {: 1.6e}  * {: 1.6e}  ! {:}\n"
                      .format(_space, basic_load.number, 
                              comb[1], factor, basic_load.name))
        _space = 24*" "
    #
    #FEMufo.append("'\n")
    #
    return FEMufo
#
#
def print_control_head(_type, EndTime=20, dT=0.1):
    """
    """
    #
    head = 'LOAD CONTROL'
    FEMufo = printing.head_line(head, comment_prefix="'")
    #
    if _type == 'dynamic':
        text='Dynamic Simulation'
        FEMufo.append("'{:} Time Domain\n".format(71 * "-"))
        FEMufo.append("'\n")
        FEMufo.append("'             EndTime          dT       dTres      dTpri\n")
        #
        FEMufo.append("STATIC     {:1.4e}  {:1.4e}  {:1.4e}  {:1.4e}  ! {:}\n"
                      .format(1, 0.1, 0.1, 0.1, 'Gravity + Buoyancy'))

        FEMufo.append("DYNAMIC    {:1.4e}  {:1.4e}  {:1.4e}  {:1.4e}  ! {:}\n"
                      .format(EndTime, dT, 0.5, 0.5, text))
        #
        FEMufo.append("'\n")
        FEMufo.append("'\n")
        FEMufo.append("'{:} Dynamic Parameters\n".format(64 * "-"))
        FEMufo.append("'\n")
        FEMufo.append("' The relative velocity between the structure and the wave particles are\n")
        FEMufo.append("' accounted for in connection with the calculation of drag forces.\n")
        FEMufo.append("' This is also the way to switch on hydro-dynamical damping.\n")
        FEMufo.append("'\n")
        FEMufo.append("' REL_VELO                ! Account for Relative Velocity\n")
        FEMufo.append("'\n")
        FEMufo.append("' With this record the user may specify the damping to be used in a Dynamic Analysis\n")
        FEMufo.append("'\n")
        FEMufo.append("'             Ratio1   Ratio2   Freq1   Freq2  History\n")
        FEMufo.append("DAMPRATIO       0.05     0.05     0.1     1.0        0 \n")
        FEMufo.append("'             Rayleigh Damping 1% at 0.1 and 10 hz\n")
        FEMufo.append("'\n")
        #
    else:
        FEMufo.append("'    This record specifies the loading history, with load & displacement control\n")
        FEMufo.append("'    parameters \n")
        FEMufo.append("'\n")
        FEMufo.append("'         nloads    npostp   mxpstp   mxpdisp \n")
        FEMufo.append("CUSFOS       10      99999    0.001     100.0 \n")
        FEMufo.append("'\n")
        FEMufo.append("'    To get the loads to reach mxld use zero nstep \n")
        FEMufo.append("'\n")
        FEMufo.append("'    Example : Storm Condition \n")
        FEMufo.append("'\n")
        FEMufo.append("'        lcomb      lfact    mxld    nstep     minstp \n")
        FEMufo.append("'             1       0.01    1.30        0      0.001   ! SelfWeight\n")
        FEMufo.append("'             2       0.01    1.06        0      0.001   ! Buoyancy\n")
        FEMufo.append("'             3       0.01    1.30        0      0.001   ! Topside\n")
        FEMufo.append("'             4       0.01    1.30        0      0.001   ! Drilling\n")
        FEMufo.append("'           500      0.001    2.00        0      0.001   ! Wave + Current + Wind\n")
        FEMufo.append("'\n")
    FEMufo.append("'\n")

    return FEMufo


#
def print_TH_header(_gravity, _buoyancy=False,
                    header='TIME HISTORY CONTROL'):
    """
    """
    FEMufo = printing.head_line(header, comment_prefix="'")

    FEMufo.append("'\n")
    FEMufo.append("'{:} Time History Control\n".format(62 * "-"))
    FEMufo.append("'\n")
    #
    FEMufo.append("'  This record is used to specify the loads to be activated during a dynamic\n")
    FEMufo.append("'  analysis with all loading controlled through time\n")
    FEMufo.append("'\n")
    #
    FEMufo.append("'            ID   <type>    T1     T2     Fac   Power\n")
    if not _gravity:
        FEMufo.append("' ")
    FEMufo.append("TIMEHIST      {:}   S_Curv     0      1       1       2  ! Ramp up DeadWeight in 1 sec\n"
                  .format(_gravity))
    # 
    if _buoyancy:
        FEMufo.append("BUOYHIST      1      All                               ! Scale buoy according to TH 1\n")
        FEMufo.append("'\n")
        FEMufo.append("'            ID   <type>  Dtime  Factor   Start_time\n")
        FEMufo.append("TIMEHIST      {:}   Switch    0.0    1.0           0.0   ! Wave\n"
                      .format(_buoyancy))
        FEMufo.append("'\n")
    #
    else:
        FEMufo.append("'\n")
        FEMufo.append("'\n")
        FEMufo.append("'            ID   <type>  Dtime  Factor   Start_time\n")
        FEMufo.append("'\n")
        FEMufo.append("' TIMEHIST      2   Switch    0.0    1.0           0.0         ! Wave\n")
        FEMufo.append("'\n")
        FEMufo.append("' TIMEHIST      3    Point    0 0  (1) 0  (2) 1  (3) 0  (4) 0  ! WiD traingular shape\n")
        FEMufo.append("'\n")
        #
    FEMufo.append("'\n")
    FEMufo.append("'{:} Time History Parameters\n".format(59 * "-"))
    FEMufo.append("'\n")
    #
    FEMufo.append("'\n")
    FEMufo.append("'          L_Case    Hist_ID\n")
    if not _gravity:
        FEMufo.append("' ")
    FEMufo.append("LOADHIST  {:7.0f}    {:7.0f}      !  Gravity\n".format(_gravity,
                                                                          _gravity))
    #
    # if _buoyancy:
    #    FEMufo.append("LOADHIST  {:7.0f}  {:7.0f}       !  Calm sea\n"
    #                 .format(_buoyancy, _buoyancy))
    #
    #
    return FEMufo


#
#
#
def print_header_meto(head):
    """
    """
    FEMufo = printing.head_line(head, comment_prefix="'")
    # Wave integration points
    FEMufo.append("'{:} Member Integration Section\n".format(56 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'    This record is used to define number of integration sections to be used in\n")
    FEMufo.append("'    connection with wave load calculations.\n")
    FEMufo.append("'\n")
    FEMufo.append("'           NIS     {Id_List}\n")
    FEMufo.append("WAVE_INT     10\n")
    FEMufo.append("'\n")
    FEMufo.append("'    NOTE ! If no elements are specified, all beam elements are using NIS\n")
    FEMufo.append("'           integration sections.\n")
    FEMufo.append("'\n")
    FEMufo.append("'{:} External Pressure Effects\n".format(57 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("'    With this record USFOS find elements for external pressure\n")
    FEMufo.append("'    (Flooded elements are skipped)\n")
    FEMufo.append("'\n")
    FEMufo.append("EXTPRES     Auto\n")
    FEMufo.append("'\n")
    
    return FEMufo
#
def print_metocean_control(metocean, _type):
    """
    """
    #
    FEMufo = []
    #
    if _type == 'dynamic':
        
        FEMufo.append("'{:} Wave, Current and Wind\n".format(60 * "-"))
        #
        i = 0
        FEMufo.append("'\n")
        # FEMufo.append("'                Type      LoadCase\n")
        for _case in metocean:
            _metocean = _case[0]
            _wave = _metocean.wave
        
            i += 1
            FEMufo.append("'\n")
            FEMufo.append("'     Wave Load Case  {:}  Wave Type : {:}\n".
                          format(_wave.number, _wave.theory))

            # Wave Kinematics reduction Factor:
            if i > 1: FEMufo.append("'")
            FEMufo.append("WAVE_KRF   {:1.4e}\n".format(_metocean.wave_kinematics))

            # Current Blocakge
            if _metocean.current:
                # FEMufo.append("'            Type          Data\n")
                if i > 1: FEMufo.append("'")
                FEMufo.append("CURRBLOCK        User   {: 1.4e}\n".format(_metocean.current_blockage))
            #
            if _metocean.buoyancy == "on":
                #
                FEMufo.append("'\n")
                FEMufo.append("'{:} Buoyancy\n".format(74 * "-"))
                FEMufo.append("'\n")
                FEMufo.append("' The buoyancy forces are added to the WAVEDATA lcase\n")
                FEMufo.append("' The buoyancy forces are updated every time wave loads are calculated,\n")
                FEMufo.append("' and the current position of the sea surface defines whether an element\n")
                FEMufo.append("' becomes buoyant or not at any time.\n")
                FEMufo.append("'\n")
                FEMufo.append("BUOYANCY                ! Account for Buoyancy\n")
                FEMufo.append("'\n")
    #
    # static sea state
    else:
        FEMufo.append("'{:} Wave, Current and Wind\n".format(60 * "-"))
        FEMufo.append("' Maximum Base Shear Default\n")
        #
        i = 0
        FEMufo.append("'\n")
        FEMufo.append("'                Type      LoadCase\n")
        
        for _case in metocean:
            i += 1
            _metocean = _case[0]
            _wave = _metocean.wave
            
            FEMufo.append("'\n")
            if i > 1: FEMufo.append("'")
            FEMufo.append("MAXWAVE     WaveLCase  {:12.0f}    ! Wave Type : {:}\n".
                          format(_metocean.number, _wave.theory))

            # Wave Kinematics reduction Factor:
            if i > 1: FEMufo.append("'")
            FEMufo.append("WAVE_KRF   {:1.4e}\n"
                          .format(_metocean.wave_kinematics))
                
            # Current Blocakge
            if _metocean.current:
                #
                if i > 1: FEMufo.append("'")
                FEMufo.append("CURRBLOCK        User   {: 1.4e}\n"
                              .format(_metocean.current_blockage))
            #
            if i > 1: FEMufo.append("'")
            if _wave.period:
                FEMufo.append("MAXWAVE     Baseshear  {:1.4e}   {:1.4e}     Write\n".
                              format(0.1, _wave.period))
                FEMufo.append("'MAXWAVE      OverTurn  {:1.4e}   {:1.4e}     Write\n".
                              format(0.1, _wave.period))
            # calm sea
            else:
                FEMufo.append("MAXWAVE     Baseshear  {:1.4e}   {:1.4e}     Write\n".
                              format(0.1, 10))
                FEMufo.append("'MAXWAVE      OverTurn  {:1.4e}   {:1.4e}     Write\n".
                              format(0.1, 10))
            # wind
            if _metocean.wind:
                _wind = _metocean.wind
                _wind_number = int(str(_metocean.number) + str(_wind.number))
                #
                if i > 1: FEMufo.append("'")
                FEMufo.append("MAXWAVE       AddLCase     {:6.0f}   ! Add wind load case and search for max\n"
                              .format(_wind_number))
            #
            #
            if _metocean.buoyancy == "on":
                #
                FEMufo.append("'\n")
                FEMufo.append("'{:} Buoyancy\n".format(74 * "-"))
                FEMufo.append("'\n")
                FEMufo.append("'    If buoyancy is specified, buoyancy effects are accounted for.\n")
                FEMufo.append("'    Note : BUOYANCY number must be redefined in COMBLOA :\n")
                FEMufo.append("'\n")
                
                
                if i > 1: FEMufo.append("'")
                FEMufo.append("'          Lcase         Acc_X        Acc_Y       Acc_Z\n")
                FEMufo.append("GRAVITY {:8.0f}   0.00000E+00  0.00000E+00 0.00000E+00   ! Define dummy load\n"
                              .format(_wave.number))
                
                FEMufo.append("'\n")
                FEMufo.append("'          Comb_Case   L_Case[i]   L1_Factor[n]\n")
                if i > 1: FEMufo.append("'")
                FEMufo.append("COMBLOAD          2     {:8.0f}   1.000000e+00 {:}! Create new COMBLOAD including dummy load\n"
                              .format(_wave.number, 10*" "))
                
                FEMufo.append("'\n")
                FEMufo.append("'           Lcase        Option\n")
                if i > 1: FEMufo.append("'")
                FEMufo.append("BUOYANCY        2         Write {:}!  Assign Bouyancy to new COMBLOAD\n"
                              .format(26*" "))
                FEMufo.append("'\n")
                    #
    FEMufo.append("'\n")

    return FEMufo
#
def print_metocean_control_seismic(metocean):
    """
    """
    FEMufo = []
    #
    # FIXME : elevation not longer here
    for _elev in metocean.elevation.values():
        for _wave in metocean.wave.values():
            if _wave.name == 'calm_sea':
                _nameWave = _wave.name
                FEMufo.append("'\n")
                #
                _type = 1.0
                FEMufo.append(
                    "'            Lcase Type    Height    Period Direction    Phase  SurfLevel  WaterDepth\n")
                #
                FEMufo.append("WAVEDATA  {:7.0f} {:1.2f} {:1.3e} {:1.3e} {:1.3e} {:1.2e} {:1.4e}  {:1.4e}\n"
                              .format(_wave.number, _type,
                                      _wave.height, _wave.period,
                                      _wave.direction, _wave.phase,
                                      _elev.surface,
                                      abs(_elev.mudline)))
                #
                FEMufo.append("'\n")
                FEMufo.append("'  Do not store data for visualization of wave\n")
                FEMufo.append("SWITCHES   WaveData      NoStore\n")
                #
                FEMufo.append("'\n")
                FEMufo.append("'  The buoyancy forces are added to the actual load case\n")
                FEMufo.append("'\n")
                FEMufo.append("'            Lcase         Option\n")
                FEMufo.append("BUOYANCY  {:7.0f}          write\n".format(_wave.number))
                FEMufo.append("'\n")
    #    
    
#
def print_metocean_TH(metocean):
    """
    """
    FEMufo = []
    FEMufo.append("'\n")

    #for _elev in metocean.elevation.values():
    i = 0
    for key in sorted(metocean, key = lambda name: metocean[name].number):
        #for _wave in metocean.wave.values():
        _metocean = metocean[key]
        _wave = _metocean.wave
        
        if _wave.theory != 'buoyancy':
            i += 1
            if i > 1: FEMufo.append("'")
            FEMufo.append("LOADHIST  {:7.0f}     {:6.0f}      ! Apply Wave Case {:}\n".
                          format(_wave.name, 2, _wave.name))

            if _metocean.wind:
                _wind = _metocean.wind
                _name = _wind.name
                if i > 1: FEMufo.append("'")
                FEMufo.append("LOADHIST  {:7.0f}     {:6.0f}      ! Apply Wind Case {:}\n".
                              format(_name, 2, _name))
            #
            FEMufo.append("'\n")
    #
    FEMufo.append("'\n")
    FEMufo.append("'\n")
    FEMufo.append("'{:} Dynamic Parameters\n".format(64 * "-"))
    FEMufo.append("'\n")
    FEMufo.append("' The relative velocity between the structure and the wave particles are\n")
    FEMufo.append("' accounted for in connection with the calculation of drag forces.\n")
    FEMufo.append("' This is also the way to switch on hydro-dynamical damping.\n")
    FEMufo.append("'\n")
    FEMufo.append("' REL_VELO                ! Account for Relative Velocity\n")
    FEMufo.append("'\n")
    FEMufo.append("' With this record the user may specify the damping to be used in a Dynamic Analysis\n")
    FEMufo.append("'\n")
    FEMufo.append("'             Ratio1   Ratio2   Freq1   Freq2  History\n")
    FEMufo.append("DAMPRATIO       0.05     0.05     0.1     1.0        0 \n")
    FEMufo.append("'             Rayleigh Damping 1% at 0.1 and 10 hz\n")
    FEMufo.append("'\n")
    FEMufo.append("'{:} Save Dynamic Results\n".format(62 * "-"))
    FEMufo.append("' - Global Results\n")
    FEMufo.append("'\n")
    FEMufo.append("DYNRES_GLOB      WaveLoad    ! Wave Forces\n")
    FEMufo.append("DYNRES_GLOB      WaveOvtm    ! Wave OverTurning Moment\n")
    FEMufo.append("DYNRES_GLOB      ReacBSH     ! Reaction, Base Shear\n")
    FEMufo.append("DYNRES_GLOB      ReacOvtm    ! Reaction, Overturning Moment\n")
    FEMufo.append("DYNRES_GLOB      WaveElev    ! Plot of Surface elevation\n")
    FEMufo.append("'\n")
    FEMufo.append("'                ResTyp   Elem Id    End  Dof\n")
    FEMufo.append("' DYNRES_ELEM     Force     53103      2    1\n")
    # FEMufo.append("'\n")
    return FEMufo


#
#
def print_time_history(TH):
    """
    """
    FEMufo = []
    FEMufo.append("'\n")
    FEMufo.append("'{:} Set Time History\n".format(60 * "-"))
    FEMufo.append("'\n")
    #
    for _no, _th in TH.items():
        FEMufo.append("' {:}\n".format(_th[0]))

        FEMufo.append("LOADHIST  {:7.0f}  {:7.0f}       !  X-direction\n"
                      .format(_no, _no))

        FEMufo.append("LOADHIST  {:7.0f}  {:7.0f}       !  Y-direction\n"
                      .format(_no + 1, _no + 1))

        FEMufo.append("LOADHIST  {:7.0f}  {:7.0f}       !  Z-direction\n"
                      .format(_no + 2, _no + 2))
    #
    return FEMufo


#
def print_seismic_soil(case, soil_depth, factors):
    """
    """
    #
    _case = 'SOILACC'
    if case == 'displacement':
        _case = 'SOILDISP'
    #
    FEMufo = []
    FEMufo.append("'\n")
    FEMufo.append("'\n")
    FEMufo.append("'  This record defines prescribed soil {:}\n"
                  .format(case))
    FEMufo.append("'\n")
    #
    _lenFactor = factors[0]
    _topLayer = 0
    #
    #
    for _depth in soil_depth:
        #  
        FEMufo.append("' {:}\n".format(_depth[1]))

        FEMufo.append("'            lCase     Type       z_Top        z_Bot  DofCode   Value\n")

        FEMufo.append("{:}   {:7.0f}        1 {: 1.4e}  {: 1.4e}        1     1.0  ! X {:}\n"
                      .format(_case, _depth[2], _topLayer, _depth[0] * _lenFactor * -1.0, case))

        FEMufo.append("{:}   {:7.0f}        1 {: 1.4e}  {: 1.4e}        2     1.0  ! Y {:}\n"
                      .format(_case, _depth[2] + 1, _topLayer, _depth[0] * _lenFactor * -1.0, case))

        FEMufo.append("{:}   {:7.0f}        1 {: 1.4e}  {: 1.4e}        3     1.0  ! Z {:}\n"
                      .format(_case, _depth[2] + 2, _topLayer, _depth[0] * _lenFactor * -1.0, case))
        #
        _topLayer = _depth[0] * _lenFactor * -1.0
        #
    #
    FEMufo.append("'\n")
    #
    return FEMufo


#
def print_seismic_node(case, node):
    """
    """
    _case = 'NODEACC'
    if case == 'displacement':
        _case = 'NODEDISP'

    FEMufo = []
    FEMufo.append("'\n")
    FEMufo.append("'\n")
    FEMufo.append("'  This record defines prescribed nodal {:} \n"
                  .format(case))
    FEMufo.append("'\n")
    #
    for _node in node:
        FEMufo.append("' {:}\n".format(_node[1]))

        FEMufo.append("{:}   {:7.0f}   {:7.0f}     1     1.0  ! X {:}\n"
                      .format(_case, _node[2], _node[0], case))

        FEMufo.append("{:}   {:7.0f}   {:7.0f}     2     1.0  ! Y {:}\n"
                      .format(_case, _node[2] + 1, _node[0], case))

        FEMufo.append("{:}   {:7.0f}   {:7.0f}     3     1.0  ! Z {:}\n"
                      .format(_case, _node[2] + 2, _node[0], case))
        #        
    #
    return FEMufo


#
# ----------------------------------------------------------------------
# TODO: don't know yet
def print_gravity(_loadNo, _grav, _dyn=1):
    """
    """
    UFOmod = []
    # _gravUnit = factors

    UFOmod.append("'\n")
    UFOmod.append("'\n")
    UFOmod.append("'{:} Gravity\n".format(75 * "-"))
    UFOmod.append("'\n")
    UFOmod.append("'         LoadCase          Acc_X        Acc_Y        Acc_Z\n")
    UFOmod.append("'\n")
    #
    UFOmod.append(" GRAVITY  {:8.0f}    0.00000E+00  0.00000E+00 {: 1.5e}\n"
                  .format(_loadNo, _grav * -1.0))
    UFOmod.append("'\n")

    if _dyn == 1:
        #
        UFOmod.append("'  This record specifies a time history for applying gravity gradually\n")
        UFOmod.append("'\n")
        UFOmod.append("'           histNo           Type           T1           T2\n")
        UFOmod.append(" TIMEHIST  {:7.0f}        S_Curve  {:1.5e}  {:1.5e}\n".
                      format(_loadNo, 0, 1.0))
        #
    return UFOmod


#
def print_calm_sea(metocean, factors, _dyn=1):
    """
    """
    print('--- Writing calm sea')

    UFOmod = []
    _lenFactor = factors[0]
    _nameWave = 0

    UFOmod.append("'{:} Calm Sea\n".format(74 * "-"))
    UFOmod.append("'\n")

    for _elev in metocean.elevation.values():
        for _key, _wave in metocean.wave.items():
            if _key == 'calm_sea':
                _nameWave = _wave.name
                UFOmod.append("'\n")
                # _type = float(getWaveType(_wave.theory))
                _type = 1.0
                UFOmod.append("'            Lcase Type    Height    Period Direction    Phase  SurfLevel  WaterDepth\n")
                # UFOmod.append("'\n")
                UFOmod.append(" WAVEDATA  {:7.0f} {:1.2f} {:1.3e} {:1.3e} {:1.3e} {:1.2e} {:1.4e}  {:1.4e}\n"
                              .format(_wave.name, _type,
                                      _wave.height * _lenFactor, _wave.period,
                                      _wave.direction, _wave.phase,
                                      _elev.surface * _lenFactor, abs(_elev.mudline) * _lenFactor))
                #
    UFOmod.append("'\n")
    UFOmod.append("'  Do not store data for visualization of wave\n")
    UFOmod.append(" SWITCHES   WaveData      NoStore\n")
    #
    UFOmod.append("'\n")
    UFOmod.append("'  The buoyancy forces are added to the actual load case\n")
    UFOmod.append("'\n")
    UFOmod.append("'            Lcase         Option\n")
    UFOmod.append(" BUOYANCY  {:7.0f}          write\n".format(_nameWave))
    UFOmod.append("'\n")
    #
    if _dyn == 1:
        # UFOmod.append("'\n")
        UFOmod.append("'  This record specifies a time history for applying wave\n")
        UFOmod.append("'\n")
        UFOmod.append("'           histNo           Type        dTime       factor      Tstart\n")
        UFOmod.append(" TIMEHIST  {:7.0f}         Switch  {:1.5e}  {:1.5e}  {:1.5e}\n"
                      .format(_nameWave, 0, 1.0, 0))
        #
    return UFOmod


#
#
#
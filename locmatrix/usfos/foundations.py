# 
# Copyright (c) 2009-2017 fem2ufo
# 


# Python stdlib imports

# package imports
import locmatrix.process.printing as printing
#import locmatrix.process.operations as operations

#
#
#
# Foundation
#
def print_pytzqz(foundation, factors):
    """
    """
    header = 'SOIL SPRING MODEL'
    UFOmod = printing.head_line(header, comment_prefix="'")
    #
    # Print PY curves
    #
    UFOmod.append("'\n")
    UFOmod.append("'{:} PY spring data\n".format(68 * "-"))
    UFOmod.append("'\n")
    springtype = 'ELPLCURV'
    
    for key in sorted(foundation, key = lambda name: foundation[name].number):
        if 'py_' in key.lower():
            _soil = foundation[key]
            UFOmod.extend(print_spring_data(_soil.spring, 
                                            springtype, factors))
    #
    # Print TZ curves
    #
    UFOmod.append("'\n")
    UFOmod.append("'{:} TZ spring data\n".format(68 * "-"))
    UFOmod.append("'\n")
    springtype = 'ELPLCURV'

    for key in sorted(foundation, key = lambda name: foundation[name].number):
        if 'tz_' in key.lower():
            _soil = foundation[key]
            UFOmod.extend(print_spring_data(_soil.spring, 
                                            springtype, factors))
    #
    # Print QZ curves
    #
    UFOmod.append("'\n")
    UFOmod.append("'{:} QZ spring data\n".format(68 * "-"))
    UFOmod.append("'\n")
    springtype = 'HYPELAST'
    
    for key in sorted(foundation, key = lambda name: foundation[name].number):
        if 'qz_' in key.lower():
            _soil = foundation[key]
            UFOmod.extend(print_spring_data(_soil.spring, 
                                            springtype, factors))
    #
    UFOmod.append("'\n")
    return UFOmod
#
#
def print_spring_data(_spring, springtype, factors):
    """
    """
    UFOmod = []
    _forceFactor = factors[4]
    _lenFactor = factors[0]
    _top = 0
    #
    _force = _spring['force']
    _disp = _spring['displacement']
    
    if _force[0] == 0.0 and _disp[0] == 0.0 :
        f_add = [item * -1 for item in _force[::-1]]
        d_add = [item * -1 for item in _disp[::-1]]
        
        _force = f_add[:len(_force)-1]
        _force.extend(_spring['force'])
        
        _disp = d_add[:len(_disp)-1]
        _disp.extend(_spring['displacement'])
    
    elif _force[1] == 0.0 and _disp[1] == 0.0 and springtype == 'HYPELAST':
        f_add = [0 for x in range(len(_force))]
        d_add = [item * -1 for item in _disp[::-1]]
        
        _force = f_add[:len(_force)-2]
        _force.extend(_spring['force'][1:])
    
        _disp = d_add[:len(_disp)-2]
        _disp.extend(_spring['displacement'][1:])        
    
    #
    UFOmod.append("'\n")
    UFOmod.append("'                  ID             P         delta\n")
    UFOmod.append("{:} {:12.0f}".format(springtype, _spring['number']))
    _spc = ''
    for x in range(len(_force)):

        if _top == _disp[x]: UFOmod.append("'")

        UFOmod.append("{:} {: 1.6e} {: 1.6e}\n"
                      .format(_spc, _force[x] * _forceFactor / _lenFactor,
                              _disp[x] * _lenFactor))
        _spc = 21 * ' '

        _top = _disp[x]

    return UFOmod


#
#
# --------------------------------------------------------------------
#
def print_foundation():
    """
    """
    head = 'FOUNDATION SECTION'
    FEMufo = printing.head_line(head, comment_prefix="'")
    FEMufo.append("'    This record select the type of spring element used with soil model\n")
    FEMufo.append("'\n")
    FEMufo.append("'    Model :\n")
    FEMufo.append("'           0 = Old nonlineal spring model, step scaling \n")
    FEMufo.append("'           1 = New plasticity formulation, no step scaling, min iteration = 1 \n")
    FEMufo.append("'\n")
    FEMufo.append("'               Model\n")
    FEMufo.append("SPRI_MOD            1\n")
    FEMufo.append("'\n")
    return FEMufo
#
#
def print_soil(foundation, factors, mudline=False):
    # units
    _lenFactor = factors[0]
    # FEMufo = []
    # FEMufo.append("'\n")
    head = 'SOIL MODELLING'
    FEMufo = printing.head_line(head, comment_prefix="'")
    #
    # --------------
    #
    FEMufo.append("'{:} Soil Properties\n".format(67 * "-"))
    FEMufo.append("'\n")
    #
    # _res = 0
    # _flag = 1
    #
    for _key1, _soil in sorted(foundation.items()):
        #
        FEMufo.append("'                  ID          Type         Z_Mud         D_ref     F_fac     L_fac\n")
        FEMufo.append("SOILCHAR {:12.0f}  {:12.0f} {: 1.6e} {: 1.6e} {:9.0f} {:9.0f}\n"
                      .format(_soil.number, 1, abs(_soil.mudline) * -1 * _lenFactor,
                              _soil.diameter * _lenFactor, 1, 1))
        #
        FEMufo.append("'\n")
        FEMufo.append("'{:28s} Z_top        Z_bott           P-Y       T-Z       Q-Z\n".format(' '))
        _layersNo = len(_soil.layers)
        #
        _top = 0
        _minSthck = 1
        _Zcoord = 0
        for _key2, _layer in sorted(_soil.layers.items()):
            #
            try:
                _test = 1.0 / _layer.thickness
                _ratioTD = _layer.thickness / _soil.diameter

                if _ratioTD < _minSthck:
                    _minSthck = _ratioTD
                    # print('==> ', _minSthck)
                    if _minSthck <= 0.1:
                        # print('==> x ', _minSthck, _layersNo + _No)
                        FEMufo.append("'{:21s} Soil layer t/D < 0.10 --> Z_bott [{:}] is modified \n"
                                      .format(' ', _layer.depth * _lenFactor))
                        _minSthck = 0.10
                        _layer.depth += (0.10 * _soil.diameter) - _layer.thickness
            #
            except ZeroDivisionError:
                FEMufo.append("'{:21s} Soil layer t = 0 --> layer omitted\n".format(' '))
                FEMufo.append("'")
            #
            FEMufo.append("{:21s} {: 1.6e} {: 1.6e}     {:9.0f} {:9.0f} {:9.0f}\n"
                          .format(' ', _top * -1 * _lenFactor, _layer.depth * -1 * _lenFactor,
                                  _layer.curve['PY'].number, 
                                  _layer.curve['TZ'].number,
                                  _layer.curve['QZ'].number))
            #
            _top = _layer.depth

        #
        # exit with additional layer
        _Zcoord = _layer.depth + _layer.thickness  # _soil.diameter
        FEMufo.append("{:21s} {: 1.6e} {: 1.6e}     {:9.0f} {:9.0f} {:9.0f}\n"
                      .format(' ', _top * -1 * _lenFactor, _Zcoord * -1 * _lenFactor,
                              _layer.curve['PY'].number, 
                              _layer.curve['TZ'].number,
                              _layer.curve['QZ'].number))
        #
        FEMufo.append("'\n")
    #
    #       
    #
    FEMufo.append("'\n")

    return FEMufo
#
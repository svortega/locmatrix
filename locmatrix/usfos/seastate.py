# 
# Copyright (c) 2009-2017 fem2ufo
# 


# Python stdlib imports
import math
from operator import itemgetter

# package imports
import locmatrix.process.printing as printing
import locmatrix.process.operations as operations

#
#
#
# hydro
#
def get_wave_type(lineIn):
    """
    Define wave theory
    """
    _key = {"1": r"airy(\s*(\_)?linear|extrapolate)?",
            "1.1": r"airy(\s*\_)?extretched",
            "2": r"stokes(\s*\_)?5",
            "3": r"user(\s*\_)?defined",
            "4": r"stream(\s*\_(function)?)?(8)?",
            "buoyancy": r"buoyancy",
            "calmsea": r"calm(\s*\_)?sea"}

    keyWord = operations.match_line(lineIn, _key)
    return keyWord
#
#
def wave_length(T, d, grav=9.80665):
    """
    Claculate aproximate wave leghth
    
    T = Wave period
    d = Water depth
    
    alpha = Wave length
    """
    Tlim = math.sqrt(2.0 * d / (grav / (2 * math.pi)))
    
    if T < Tlim:
        alpha = grav * T ** 2 / (2 * math.pi)
    else:
        alpha = 2.0 * d * (2.0 * T / Tlim - 1.0)

    alpha = round(alpha)

    return alpha
#
# Metocean
#
def print_header_metocean():
    """
    Print metocean header
    """
    header = 'METOCEAN DATA'
    UFOmod = printing.head_line(header, comment_prefix="'")
    return UFOmod

#
#
def print_wave_current(metocean, mudline, wdepths, factors):
    """
    Print wave and current cards is usfos format
    """
    surface = wdepths - mudline
    
    UFOmod = []
    # m
    _lenFactor = factors[0]
    # fix this m/s
    _velFactor = factors[0]
    # 
    UFOmod.append("'{:} Wave and Current\n".format(66 * "-"))
    UFOmod.append("'\n")
    UFOmod.append("'\n")
    UFOmod.append("'    WAVEDATA\n")
    UFOmod.append("'    With this record, the user may specify an irregular wave to be applied \n")
    UFOmod.append("'    to the structure as hydrodynamic forces\n")
    UFOmod.append("'\n")
    UFOmod.append("'    CURRENT\n")
    UFOmod.append("'    With this record, the user may specify a current to be applied to the\n")
    UFOmod.append("'    structure as hydrodynamical forces.\n")
    UFOmod.append("'\n")
    #
    #for _elev in metocean.elevation.values():
    # wave
    for key in sorted(metocean, key = lambda name: metocean[name].number):
    #for _metocean in metocean.values():
        _metocean = metocean[key]
        if _metocean.wave:
            _wave = _metocean.wave
            if abs(_wave.water_depth) == abs(wdepths):
                Tw = _wave.period
                dw = abs(mudline) * _lenFactor
                # calm sea
                if  'buoy' in _wave.theory.lower() or 'calm' in _wave.theory.lower() :
                    UFOmod.append("'\n")
                    UFOmod.append("'           Lcase Type   Height    Period  Direction  Phase   SurfLevel  WaterDep  N\n")
                    UFOmod.append("'                          (m)       (s)     (deg)    (deg)      (m)        (m)\n")
                    UFOmod.append("WAVEDATA  {:7.0f} Airy      0.0      10.0       0       0.0   {:1.3e}  {:1.3e}\n"
                                  .format(_metocean.number, surface * _lenFactor, dw))
                # regular wave
                else:
                    UFOmod.append("'\n")
                    _type = float(get_wave_type(_wave.theory))
                    UFOmod.append("'           Lcase Type   Height    Period  Direction  Phase   SurfLevel  WaterDep  N\n")
                    UFOmod.append("'                          (m)       (s)     (deg)    (deg)      (m)        (m)\n")
                    UFOmod.append("WAVEDATA  {:7.0f} {:1.2f} {:1.3e} {:1.3e} {:1.3e} {:1.2e} {:1.3e}  {:1.3e}"
                                  .format(_metocean.number, _type,
                                          _wave.height * _lenFactor, Tw,
                                          _metocean.wave_direction, _wave.phase,
                                          surface * _lenFactor, dw))
                
                    UFOmod.append(" '6\n")
                    WaveLength = wave_length(Tw, dw)
                    UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", -2.0 * WaveLength, 0))
                    UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", -1.5 * WaveLength, 0))
                    UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", -1.0 * WaveLength, 1))
                    UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", -0.5 * WaveLength, 1))
                    UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", 0.0 * WaveLength, 0))
                    UFOmod.append("'{:} {: 1.3e}  {:1.3e}\n".format(60 * " ", 0.5 * WaveLength, 0))
                    UFOmod.append("'\n")                    
                #    
                # current
                if _metocean.current:
                    _curr = _metocean.current
                    _max = max(_curr.profile, key=itemgetter(1))[1]
                    _curr.velocity = _max
                    _temp = []
                    for _items in _curr.profile:
                        try:
                            _temp.append((_items[0], _items[1] / _max))
                        # TODO : check if this works
                        except ZeroDivisionError:
                            _temp.append((_items[0], 0))
                    #
                    _curr.profile = _temp            
                    #
                    UFOmod.append("'\n")
                    UFOmod.append("'           Lcase       Speed   Direction SurfLevel WaterDept      [Profile]\n")
                    UFOmod.append("'                       (m/s)     (deg)     (m)        (m)    Zcoord(m)   Factor\n")
                    UFOmod.append("CURRENT   {:7.0f}     {:1.3e} {:1.3e} {:1.3e} {:1.3e}"
                                  .format(_metocean.number, _curr.velocity * _velFactor,
                                          _metocean.current_direction,
                                          surface * _lenFactor, 
                                          abs(mudline) * _lenFactor))
                    i = 0
                    _currentProfile = reversed(_curr.profile)
                    for _item in _currentProfile:
                        i += 1
                        if i > 1: UFOmod.append("{:}".format(61 * " "))
                        UFOmod.append(
                            "{: 1.3e} {: 1.3e}\n".format((_item[0] - abs(mudline)) * _lenFactor, _item[1]))
            
                UFOmod.append("'\n")
    #
    # UFOmod.append("'\n")
    # UFOmod.append("'{:} Buoyancy\n".format(74*"-"))
    # UFOmod.append("'\n")
    # UFOmod.append("'    If buoyancy is specified, buoyancy effects are accounted for.\n")
    # UFOmod.append("'\n")
    # UFOmod.append("'           Lcase        Option\n")
    # for _wave in metocean.wave.values():
    #    if _wave.theory == 'buoyancy':
    #        UFOmod.append("BUOYANCY {:8.0f}         Write\n"
    #                     .format(_wave.name))    
    #
    UFOmod.append("'\n")
    #
    #
    # print('ok')
    return UFOmod
#
#
def print_wind(metocean, air_density, factors):
    """
    Print wind parameters in usfos format
    """
    #  Units
    _lenFactor = factors[0]
    # fix this m/s
    _velFactor = factors[0]
    _density = factors[1] / factors[0] ** 3
    #
    # UFOmod = []
    header = 'WIND DATA'
    UFOmod = printing.head_line(header, comment_prefix="'")
    #
    UFOmod.append("'   With this record, the user may specify a windfield to be applied to the\n")
    UFOmod.append("'   structure as aerodynamical forces.\n")
    UFOmod.append("'\n")
    #
    for key in sorted(metocean, key = lambda name: metocean[name].number):
    #for _metocean in metocean.values():
        _metocean = metocean[key]
        # 
        if _metocean.wind:
            _wind = _metocean.wind
            _wind_number = int(str(_metocean.number) + str(_wind.number))
            #_name = _wind.name
            UFOmod.append("'\n")
            _ux = _wind.velocity * _velFactor * math.cos(math.radians(_metocean.wind_direction))
            _uy = _wind.velocity * _velFactor * math.sin(math.radians(_metocean.wind_direction))
            UFOmod.append(
                "'   ux = {:1.3e}*COS({:}*PI/180),".format(_wind.velocity * _velFactor, 
                                                           _metocean.wind_direction))
            UFOmod.append(
                "    uy = {:1.3e}*SIN({:}*PI/180)\n".format(_wind.velocity * _velFactor, 
                                                            _metocean.wind_direction))
            UFOmod.append("'           Lcase   Type   ux (m/s)   uy (m/s)    uz   Z_0  Zbot Rho(kg/m3)    Power\n")
            UFOmod.append("WINDFIELD {:7.0f} Z_prof {: 1.3e} {: 1.3e} 0.000 0.000 0.000 {:1.3e} {:1.3e}\n"
                          .format(_wind_number, _ux, _uy,
                                  air_density * _density, _wind.power))
            # _wave.wind.exponent))
        #
    #
    UFOmod.append("'\n")
    return UFOmod
#
#
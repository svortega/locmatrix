# 
# Copyright (c) 2009-2017 fem2ufo
#
#
# Python stdlib imports
#import os

# package imports
import locmatrix.xl.read_xl as xlread
import locmatrix.usfos.foundations as foundations
import locmatrix.usfos.seastate as seastate
import locmatrix.usfos.loading as ufo_load
import locmatrix.process.printing as printing
#import locmatrix.process.operations as operations

#
#
def print_foundation(soil, springs, factors, soil_name):
    """
    """        
    #
    ufo_soil = foundations.print_foundation()
    ufo_soil.extend(foundations.print_soil(soil, factors))
    ufo_soil.extend(foundations.print_pytzqz(springs, factors))
    
    file_out = soil_name + '_ufo.fem'
    add_out = open(file_out, 'w+')
    add_out.write("".join(ufo_soil))
    add_out.write("".join(printing.EOF(comment_prefix="'")))
    add_out.close()
    #
#
def print_metocean(analysis, mudline,
                   case_name, factors):
    """
    """
    #
    _elevations = {}
    air_density = analysis.condition.air_density
    i = 0
    for wdepths in analysis.condition.water_depth:
        i+= 1
        _name = case_name + '_' + str(i)
        _elevations[_name] = seastate.print_header_metocean()
        _elevations[_name].extend(seastate.print_wave_current(analysis.metocean_combination, 
                                                              mudline, wdepths, factors))
        _elevations[_name].extend(seastate.print_wind(analysis.metocean_combination, 
                                                      air_density, factors))
    #
    for _name, _elev in _elevations.items():
        file_add = _name + '_ufo.fem'
        wave_out = open(file_add, 'w+')
        wave_out.write("".join(_elev))
        wave_out.write("".join(printing.EOF(comment_prefix="'")))
        wave_out.close()
    #
#
def print_load_combinations(combination, level_1, functional,
                            _number, _type):
    """
    """
    header = 'LOAD COMBINATIONS SECOND LEVEL'
    ufo_lcomb = printing.head_line(header, comment_prefix="'")
    ufo_lcomb.append("'\n")
    ufo_lcomb.append("'    This record defines USFOS load combinations \n")
    ufo_lcomb.append("'\n")
    ufo_lcomb.append("' L2 Combination Name   : {:}\n".format(combination.name))
    ufo_lcomb.append("'                Number : {:}\n".format(combination.number))
    ufo_lcomb.append("'\n")
    #
    # Print metocean
    _step = 0
    for comb in combination.functional_load:
        basic = level_1[comb[0]]
        if basic.metocean_load:
            _step += 1
            if _step == 1:
                head = 'METOCEAN DATA'
                ufo_lcomb.extend(ufo_load.print_header_meto(head))
            #
            _temp = ufo_load.print_metocean_control(basic.metocean_load, _type)
            ufo_lcomb.extend(_temp)
    #
    # Print functional load
    _step = 0
    for comb in combination.functional_load:
        basic = level_1[comb[0]]
        if basic.functional_load:
            _number += 1
            _step += 1
            if _step == 1:
                head = 'LOAD COMBINATION'
                ufo_lcomb.extend(printing.head_line(header, comment_prefix="'"))
            #
            ufo_lcomb.extend(ufo_load.print_combination(comb[1], basic, 
                                                        functional, _number))
    #
    return ufo_lcomb, _number
#
def print_ufo(factored_sheet, actual_sheet, 
              metocean_sheet, soil_sheet,
              analysis_case, design_condition, 
              mudline, water_depth):
    """
    """
    #
    surface = mudline + water_depth
    factors = [1, 1, 1, 1, 1, 1]
    #
    # Print foundation
    #
    if soil_sheet[0]:
        foundation, springs = xlread.get_foundation(soil_sheet[0])

        if soil_sheet[1]:
            print_foundation(foundation, springs, 
                             factors, analysis_case[2])
    else:
        foundation = None
        analysis_case[2] = None
    #
    # Print Metocean
    #
    ufo_meto = []
    if metocean_sheet[0] :
        metocean =  xlread.get_metocean(metocean_sheet[0])
        name = analysis_case[1]
        analysis = metocean[name]
        metocean_combination = analysis.metocean_combination
        water_depths = analysis.condition.water_depth
        
        if metocean_sheet[1]:
            ufo_meto = print_metocean(analysis, mudline, 
                                      analysis_case[1], factors)
    else:
        metocean_combination = None
        analysis_case[1] = None
        water_depths = [water_depth]
    #
    # Print Load Combinations
    #
    _comb_name = analysis_case[0]
    ufo_file = []
    _number = 2
    _type = 'static'
    #
    level_1, functional = xlread.get_basic_combinations(actual_sheet[0], 
                                                        metocean_combination,
                                                        design_condition[0])
    # Second Level combination
    if factored_sheet[1]:
        level_2, level_3 = xlread.get_factored_combinations(factored_sheet[0], 
                                                            design_condition[0])
        
        Load_case = level_2[_comb_name]
        
        _temp, _number = print_load_combinations(Load_case, level_1, functional,
                                                 _number, _type)
        ufo_file.extend(_temp)
        
        file_out = str(factored_sheet[0]) + '.fem'
        #
        ufo_file.extend(ufo_load.print_control_head(_type))
        #
        add_out = open(file_out, 'w+')
        add_out.write("".join(ufo_file))
        add_out.close()
    # First Level combination
    elif actual_sheet[1]:
        _number += 1
        header = 'LOAD COMBINATIONS FIRTS LEVEL'
        ufo_file = printing.head_line(header, "'")
        Load_case = level_1[_comb_name]
        ufo_file.extend(ufo_load.print_basic_load_combination(Load_case, 
                                                              functional, _number))        
        #
        file_out = str(actual_sheet[0]) + '.fem'
        #
        ufo_file.extend(ufo_load.print_control_head(_type))
        #
        add_out = open(file_out, 'w+')
        add_out.write("".join(ufo_file))
        add_out.close()        
    #
    #
    #print('end ufo')
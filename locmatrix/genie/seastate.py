# 
# Copyright (c) 2009-2016 fem2ufo
# 


# Python stdlib imports
import math
from operator import itemgetter

# package imports
import locmatrix.process.printing as printing
import locmatrix.process.operations as operations


#
#
def get_buoyancy(lineIn):
    """
    """
    _key = {"buOff": r"\b((bu\s*)?off)\b",
            "buOn": r"\b((bu\s*)?on)\b",
            "buOnly": r"\b((bu\s*)?only)\b",
            "mgWeight": r"\b((mg\s*)?weight)\b"}
    keyWord, lineOut, _match =operations.select(lineIn, _key)
    return keyWord
#
#
def print_enviroment_soil(sea_surface, mudline, foundation,
                          factors, location_name=None):
    """
    """
    if  location_name:
        location_name = location_name.strip()
    else:
        location_name = "Location_1"
    
    _length_factor = factors[0]
    #
    header = 'ENVIRONMENT'
    JSmod = printing.head_line(header, comment_prefix='//')    
    header = 'Locations'
    JSmod.extend(printing.section_head(header, comment_prefix='//'))
    
    for _number, _soil in foundation.items():
        # water surface & mudline
        JSmod.append("{:} = Location({:} m, {:} m);\n"
                     .format(location_name, round(sea_surface * _length_factor, 3), 
                             round(math.copysign(_soil.mudline, mudline) * _length_factor, 3)))
        JSmod.append("{:}.gravity = 9.80665 m/s^2;\n"
                     .format(location_name))
        JSmod.append("{:}.air().density = 1.226 Kg/m^3;\n"
                     .format(location_name))
        JSmod.append("{:}.air().kinematicViscosity = 1.462e-005 m^2/s;\n"
                     .format(location_name))
        JSmod.append("{:}.water().density = 1030 Kg/m^3;\n"
                     .format(location_name))
        JSmod.append("{:}.water().kinematicViscosity = 1.899e-006 m^2/s;\n"
                     .format(location_name))
        JSmod.append("{:}.seabed().normaldirection = Vector3d(0 m,0 m,1 m);\n"
                     .format(location_name))
        #
        #
        _borders = [[key, (abs(mudline) + _layer.depth)]
                    for key, _layer in _soil.layers.items()]
        
        #_borders = sorted(_borders)
        _borders = sorted(_borders,key=itemgetter(1))
        # 
        JSmod.append("//\n")
        for _border in _borders:
            JSmod.append("{:}.insertSoilBorder({:} m);\n"
                         .format(location_name,
                                 round(math.copysign(_border[1], mudline) * _length_factor, 3)))
        
        JSmod.append("//\n")
        #
        _step = 0
        _diam = _soil.diameter
        
        for x in range(len(_borders)-1):
            _layer = _soil.layers[_borders[x][0]]
            _border = _borders[x+1][1] - _borders[x][1]
            
            _sublayer = _layer.sublayers or max(round(_border/_diam), 1)
            
            _step += 1
            JSmod.append("{:}.soil({:}).soilCurves = {:}_{:};\n"
                         .format(location_name, _step, _soil.name, _step))
            JSmod.append("{:}.soil({:}).soilData = SoilData_{:};\n"
                         .format(location_name, _step, int(_number)))
            JSmod.append("{:}.soil({:}).soilType = Soil_default;\n"
                         .format(location_name, _step))
            JSmod.append("{:}.soil({:}).numberOfSublayers = {:};\n"
                         .format(location_name, _step, _sublayer))
        #
        _layer = _soil.layers[_borders[_step][0]]
        _border = _borders[_step][1] - _borders[_step-1][1]
        _sublayer = _layer.sublayers or max(round(_border/_diam), 1)
        _step += 1
        JSmod.append("{:}.soil({:}).soilCurves = {:}_{:};\n"
                     .format(location_name, _step, _soil.name, _step))
        JSmod.append("{:}.soil({:}).soilData = SoilData_{:};\n"
                     .format(location_name, _step, int(_number)))
        JSmod.append("{:}.soil({:}).soilType = Soil_default;\n"
                     .format(location_name, _step))
        JSmod.append("{:}.soil({:}).numberOfSublayers = {:};\n"
                     .format(location_name, _step, _sublayer))
    #
    JSmod.append("//\n")
    JSmod.append("//\n")
    return JSmod
#
#
def print_enviroment_no_soil(sea_surface, mudline,
                             factors, location_name=None):
    """
    """
    #
    if  location_name:
        location_name = location_name.strip()
    else:
        location_name = "Location_1"   
    #
    _length_factor = factors[0]
    
    header = 'ENVIRONMENT'
    JSmod = printing.head_line(header, comment_prefix='//')    
    header = 'Locations'
    JSmod.extend(printing.section_head(header, comment_prefix='//'))    
    #
    JSmod.append("{:} = Location({:} m, {:} m);\n"
                 .format(location_name, sea_surface * _length_factor,
                         mudline * _length_factor))
    JSmod.append("{:}.gravity = 9.80665 m/s^2;\n"
                 .format(location_name))
    JSmod.append("{:}.air.density = 1.226 Kg/m^3;\n"
                 .format(location_name))
    JSmod.append("{:}.air.kinematicViscosity = 1.462e-005 m^2/s;\n"
                 .format(location_name))
    JSmod.append("{:}.water.density = 1025 Kg/m^3;\n"
                 .format(location_name))
    JSmod.append("{:}.water.kinematicViscosity = 1.19e-006 m^2/s;\n"
                 .format(location_name))
    JSmod.append("{:}.seabed.normaldirection = Vector3d(0 m,0 m,1 m);\n"
                 .format(location_name))
    JSmod.append("{:}.relativeSoilLayers = false;\n"
                 .format(location_name))
    JSmod.append("//\n")
    JSmod.append("//\n")
    return JSmod
#
#
def print_soil_no_enviroment(foundation, mudline, factors,
                             location_name=None):
    """
    """
    #
    if  location_name:
        location_name = location_name.strip()
    else:
        location_name = "Location_1"  
    #
    _length_factor = factors[0]
    set_no = 1
    
    header = 'ENVIRONMENT'
    JSmod = printing.head_line(header, comment_prefix='//')    
    header = 'Locations'
    JSmod.extend(printing.section_head(header, comment_prefix='//'))
    #
    for _number, _soil in foundation.items():
        _sign = math.copysign(0.01, mudline)
        sea_surface = round(mudline + (mudline * _sign))
        
        JSmod.append("{:} = Location({:}m, {:}m);\n"
                     .format(location_name, 
                             sea_surface  * _length_factor, 
                             mudline  * _length_factor))
        JSmod.append("{:}.gravity = 9.80665 m/s^2;\n"
                     .format(location_name))
        JSmod.append("{:}.air.density = 1.226 Kg/m^3;\n"
                     .format(location_name))
        JSmod.append("{:}.air.kinematicViscosity = 1.462e-005 m^2/s;\n"
                     .format(location_name))
        JSmod.append("{:}.water.density = 1025 Kg/m^3;\n"
                     .format(location_name))
        JSmod.append("{:}.water.kinematicViscosity = 1.19e-006 m^2/s;\n"
                     .format(location_name))
        JSmod.append("{:}.seabed.normaldirection = Vector3d(0 m,0 m,1 m);\n"
                     .format(location_name))
        #
        #
        _borders = [(abs(mudline) + _layer.depth)
                    for _layer in _soil.layers.values()]
        _borders = sorted(_borders)
        
        JSmod.append("//\n")
        for _border in _borders:
            JSmod.append("{:}.insertSoilBorder({:} m);\n"
                         .format(location_name,
                                 round(math.copysign(_border, mudline) * _length_factor, 3)))
        
        JSmod.append("//\n")
        _step = 0
        for _step in range(1, len(_borders)+1):
            
            JSmod.append("{:}.soil({:}).soilCurves = {:}_{:};\n"
                         .format(location_name, _step, _soil.name, _step))
            JSmod.append("{:}.soil({:}).soilData = SoilData_{:};\n"
                         .format(location_name, _step, int(_number)))
            JSmod.append("{:}.soil({:}).soilType = Soil_default;\n"
                         .format(location_name, _step))
            JSmod.append("{:}.soil({:}).numberOfSublayers = 1;\n"
                         .format(location_name, _step))        
    #
    JSmod.append("//\n")
    JSmod.append("//\n")
    return JSmod
#
#
def print_conditions(_metocean, analysis_name):
    """
    """
    #
    seastate_name = analysis_name[1]
    location_name = 'Location_' + analysis_name[0]
    #
    _wave_list = [key for key in sorted(_metocean, key = lambda name: _metocean[name].number)
                  if not 'calm' in _metocean[key].wave.theory.lower()]
    #
    header = 'CONDITIONS'
    JSmod = printing.head_line(header, comment_prefix='//')     
    #
    JSmod.append("{:} = DeterministicTime({:});\n"
                 .format(seastate_name, location_name))      
    #
    if _wave_list:
        JSmod.append("{:}.waterSurface().regularWaveSet = Wave_{:};\n"
                     .format(seastate_name, seastate_name))
        
        JSmod.append("{:}.water().setNoCurrent();\n".format(seastate_name))
    #
    JSmod.append("{:}.populate();\n".format(seastate_name))
    JSmod.append("//\n")
    #
    #
    i = 0
    for key in _wave_list:
        i += 1
        _seastate = _metocean[key]
        _wave = _seastate.wave
        _waveTheory = _wave.theory
        #
        # wave
        if 'stream' in _waveTheory.lower():
            JSmod.append("{:}.component({:}).waterSurface.waveModel({:}({:}));\n"
                         .format(seastate_name, i, _waveTheory, int(_wave.order)))
        else:
            JSmod.append("{:}.component({:}).waterSurface.waveModel({:}());\n"
                         .format(seastate_name, i, _waveTheory))
        #
        # current
        if _seastate.current:
            JSmod.append("{:}.component({:}).water().current = Current_{:}_{:};\n"
                         .format(seastate_name, i, _seastate.number, seastate_name))
            JSmod.append("{:}.component({:}).water().current(Current_{:}_{:});\n"
                         .format(seastate_name, i, _seastate.number, seastate_name))
        else:
            JSmod.append("{:}.component({:}).water().setNoCurrent();\n"
                         .format(seastate_name, i))
        #
        # wind
        if _seastate.wind:
            JSmod.append("{:}.component({:}).air.windProfile(Wind_{:}_{:});\n"
                         .format(seastate_name, i, _seastate.number, seastate_name))
    #
    #
    _calm_list = [key for key in sorted(_metocean, key = lambda name: _metocean[name].number)
                  if 'calm' in _metocean[key].wave.theory.lower()]    
    #
    for key in _calm_list:
        i += 1
        _seastate = _metocean[key]
        JSmod.append("{:}.addCalmSea();\n"
                             .format(seastate_name))            
        #
        # current
        if _seastate.current:
            JSmod.append("{:}.component({:}).water().current = Current_{:}_{:};\n"
                         .format(seastate_name, i, _seastate.number, seastate_name))
            JSmod.append("{:}.component({:}).water().current(Current_{:}_{:});\n"
                         .format(seastate_name, i, _seastate.number, seastate_name))
        else:
            JSmod.append("{:}.component({:}).water().setNoCurrent();\n"
                         .format(seastate_name, i))
        #
        # wind
        if _seastate.wind:
            JSmod.append("{:}.component({:}).air.windProfile(Wind_{:}_{:});\n"
                         .format(seastate_name, i, _seastate.number, seastate_name))
        #
    #    
    #
    JSmod.append("//\n")
    JSmod.append("//\n")
    return JSmod
#
#
def print_no_conditions(seastate_name=None):
    """
    """
    #
    if  seastate_name:
        seastate_name = seastate_name.strip()
    else:
        seastate_name = "1"  
    #
    header = 'CONDITIONS'
    JSmod = printing.head_line(header, comment_prefix='//')
    #
    JSmod.append("{:} = DeterministicTime(Location_{:});\n"
                 .format(seastate_name, seastate_name))     
    JSmod.append("{:}.addCalmSea();\n"
                 .format(seastate_name))
    JSmod.append("{:}.water().setNoCurrent();\n"
                 .format(seastate_name))
    JSmod.append("//\n")
    JSmod.append("//\n")
    return JSmod
#
#
def print_analysis_metocean(_metocean, water_depths, 
                            mudline, minimum_depth, 
                            analysis_name):
    """
    """
    #
    seastate_name = analysis_name[0]
    condition_name = analysis_name[1]
    #
    #minimum_depth = abs(mudline - surface)
    #
    header = 'ANALYSIS'
    JSmod = printing.head_line(header, comment_prefix='//')    
    #
    JSmod.append("{:} = Analysis(true);\n".format(seastate_name))
    JSmod.append("{:}.add(MeshActivity());\n".format(seastate_name))
    JSmod.append("{:}.step(1).beamsAsMembers = true;\n".format(seastate_name))
    JSmod.append("{:}.step(1).smartLoadCombinations = true;\n".format(seastate_name))
    JSmod.append("{:}.step(1).usePartialMesher = false;\n".format(seastate_name))
    JSmod.append("{:}.step(1).lockMeshedConcepts = true;\n".format(seastate_name))
    
    if analysis_name[2]:
        JSmod.append("{:}.step(1).pileBoundaryCondition = pmPileSoilInteraction;\n".format(seastate_name))
        JSmod.append("{:}.step(1).superElementTypeTop = 21;\n".format(seastate_name))
        JSmod.append("{:}.step(1).superElementType = 1;\n".format(seastate_name))        
    else:
        JSmod.append("{:}.step(1).pileBoundaryCondition = pmFixed;\n".format(seastate_name))
        JSmod.append("{:}.step(1).setNoSuperElementType();\n".format(seastate_name))
    #
    JSmod.append("{:}.step(1).nodeNumberFromJointName = false;\n".format(seastate_name))
    JSmod.append("{:}.step(1).elementNumberFromBeamName = false;\n".format(seastate_name))
    JSmod.append("{:}.step(1).regenerateMeshOption = anConditionalRegenerateMesh;\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.add(WaveLoadActivity({:}));\n"
                 .format(seastate_name, condition_name))
    JSmod.append("{:}.step(2).loadCalculation(true);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).specialOptions().useMacCamyFuchs(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).specialOptions().inPhase(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).specialOptions().useOnStructure(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).specialOptions().excludeEccentricities(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).specialOptions().useEliminatedStructure(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).specialOptions().absoluteWaterDepths(false);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    #
    for wdepths in water_depths:
        _level = abs(round(abs(wdepths) - abs(minimum_depth), 3))
        if _level <= 0.001:
            continue
        JSmod.append("{:}.step(2).specialOptions().addLevel({:1.4e} m);\n".format(seastate_name, _level))
    #
    JSmod.append("{:}.step(2).deterministicSeastates().setStepType(PhaseStepping);\n".format(seastate_name))
    JSmod.append("{:}.step(2).deterministicSeastates().gustWind(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).deterministicSeastates().populate();\n".format(seastate_name))  
    #
    # print wave only
    wave_number = []
    i = 0
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        _wave = _seastate.wave
        wave_number.append(_seastate.number)
        #
        if 'calm' in _wave.theory.lower():
            continue
        #
        i += 1
        # doopler effect
        JSmod.append("{:}.step(2).deterministicSeastates().seastate({:}).doppleroption(doUseRules);\n"
                     .format(seastate_name, i))
        #
        JSmod.append("{:}.step(2).deterministicSeastates().seastate"
                     .format(seastate_name))
        
        stretching = 'NoStretching'
        if _seastate.current_stretching:
            stretching = 'WheelerStretching'
        #
        buoyancy = get_buoyancy(_seastate.buoyancy)
        #
        if analysis_name[2]:
            _depth = abs(minimum_depth)
            if _wave.water_depth:
                _depth = str(abs(_wave.water_depth)) + ' m'
        else:
            _depth = ""  # no water depth is needed
        #
        JSmod.append("({:}).dataPhase({:}, 5 deg, 72, {:}, {:}, {:}, {:}, {:});\n"
                     .format(i, stretching, 
                             buoyancy, _seastate.design_load,
                             _seastate.current_blockage, 
                             _seastate.wave_kinematics, _depth))
        #
        _seastate.number = i
    #
    #
    # print calm sea only
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        _wave = _seastate.wave         
        
        if 'calm' in _wave.theory.lower():
            i += 1
            #
            # check if foundation exist
            if analysis_name[2]:
                _depth = abs(minimum_depth)
                if _wave.water_depth:
                    _depth = str(abs(_wave.water_depth)) + ' m'
            else:
                #if i == 1: # check if no detrministic wave print special option
                #    JSmod.append("{:}.step(2).specialOptions().addLevel({:1.4e} m);\n".format(seastate_name, _level))
                _depth = ""  # no water depth is needed
            #
            # doopler effect
            JSmod.append("{:}.step(2).deterministicSeastates().seastate({:}).doppleroption(doOff);\n"
                         .format(seastate_name, i))
            #
            JSmod.append("{:}.step(2).deterministicSeastates().seastate"
                         .format(seastate_name))
            #
            buoyancy = get_buoyancy(_seastate.buoyancy)
            
            JSmod.append("({:}).dataPhase(NoStretching, 0 deg, 1, {:}, {:}, {:}, 1, {:});\n"
                         .format(i, buoyancy, _seastate.design_load,
                                 _seastate.current_blockage, _depth))
            #
            _seastate.number = i
            #
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).addedMassAndDamping().calculateAddedMass(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().includeInternalWater(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().useCm(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().includeLongitudinalAddedMass(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().includeLongitudinalHydrodynamicMass(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().calculateDamping(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).addedMassAndDamping().includeLongitudinalDamping(false);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).rules().setRuleType(wrNone);\n".format(seastate_name))
    JSmod.append("{:}.step(2).rules().includeDoppler(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).rules().applyWakeType(wrNoApply);\n".format(seastate_name))
    JSmod.append("{:}.step(2).rules().maxAngle(0 deg);\n".format(seastate_name))
    JSmod.append("{:}.step(2).rules().adjustForCurrent(false);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).buoyancy().horisontalFreeSurface(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).buoyancy().steelAreaBuoyancy(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).buoyancy().includeEndForces(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).buoyancy().beamsAtMudlineBuoyancy(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).buoyancy().includeWeightOfMarineGrowth(true);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).morison().normalToElement(true);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).output().globalResults().momentRefPoint(Point(0 m,0 m,{:} m));\n"
                 .format(seastate_name, mudline))
    JSmod.append("{:}.step(2).output().globalResults().generateSIFFile(false);\n".format(seastate_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.step(2).output().loadsInterfaceFile().generateLFile(true);\n".format(seastate_name))
    #if water_depths:
    #    JSmod.append("{:}.step(2).output().loadsInterfaceFile().formatted(true);\n".format(seastate_name))
    #else:
    JSmod.append("{:}.step(2).output().loadsInterfaceFile().formatted(false);\n".format(seastate_name))
    JSmod.append("{:}.step(2).output().loadsInterfaceFile().loadCaseNumbering(true);\n".format(seastate_name))
    JSmod.append("{:}.step(2).output().loadsInterfaceFile().firstLoadCaseNumber({:});\n"
                 .format(seastate_name, min(wave_number)))
    #
    #
    if analysis_name[2]:
        JSmod.append("//\n")
        JSmod.append("{:}.add(PileSoilAnalysis({:}));\n"
                     .format(seastate_name, condition_name))
        JSmod.append("{:}.step(3).soil.materialCoeffs.tanPhiCoeff = 1;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.materialCoeffs.shearStrengthCoeff = 1;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.materialCoeffs.skinFrictionCoeff = 1;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.materialCoeffs.pileTipResistanceCoeff = 1;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.curveGeneration.lowestShearStiff = 100 Pa;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.curveGeneration.lowestLevelWithCyclicPY = 0 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.curveGeneration.zoneOfInfluenceTZ = 10;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.curveGeneration.curveShapeFactorTZ = 0.9;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.groupEffects.averagePoissonRatio = 0.5;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.groupEffects.modulusOfElasticity(10000 Pa, 0 m, 10000 Pa, -100 m);\n"
                     .format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.verticalStressAtSurface = 0 KPa;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.verticalStressUnderEmbankment = 0 Pa;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.widthOfEmbankmentSlopingPart = 0 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.distancePileToEmbankmentToe = 0 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.verticalStressUnderCircularLoadedArea = 0 Pa;\n".format(seastate_name))
        JSmod.append("{:}.step(3).soil.loadsAtSurface.radiusOfCircularLoadedArea = 0 m;\n".format(seastate_name))
        #
        JSmod.append("//\n")
        JSmod.append("{:}.step(3).splice.solver.maxIterations = 20;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.solver.convergenceCriterion = 0.001 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.solver.divergenceCheck = false;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.solver.simplifiedGroupEffects = false;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.solver.revisionV = false;\n".format(seastate_name))
        
        JSmod.append("{:}.step(3).splice.groupEffects.interactionCode = anOff;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.groupEffects.interactionDistance = 0 m;\n".format(seastate_name))
        JSmod.append("{:}.step(3).splice.linearisedSprings.computeLinearisedSprings = false;\n".format(seastate_name))
        #
        JSmod.append("//\n")
        JSmod.append("{:}.step(3).sestra.warpCorrection = true;\n".format(seastate_name))
        JSmod.append("{:}.step(3).sestra.continueOnError = false;\n".format(seastate_name))
        JSmod.append("//\n")
    else:
        JSmod.append("//\n")
        JSmod.append("{:}.add(LinearAnalysis());\n".format(seastate_name))
    #
    #
    JSmod.append("{:}.add(LoadResultsActivity());\n".format(seastate_name))
    return JSmod
#
#
def print_analysis_no_metocean(analysis_name):
    """
    """
    header = 'ANALYSIS'
    JSmod = printing.head_line(header, comment_prefix='//')
    #
    JSmod.append("{:} = Analysis(true);\n".format(analysis_name))
    JSmod.append("{:}.add(MeshActivity());\n".format(analysis_name))
    JSmod.append("{:}.step(1).beamsAsMembers = true;\n".format(analysis_name))
    JSmod.append("{:}.step(1).smartLoadCombinations = true;\n".format(analysis_name))
    JSmod.append("{:}.step(1).usePartialMesher = false;\n".format(analysis_name))
    JSmod.append("{:}.step(1).lockMeshedConcepts = true;\n".format(analysis_name))
    JSmod.append("{:}.step(1).nodeNumberFromJointName = false;\n".format(analysis_name))
    JSmod.append("{:}.step(1).elementNumberFromBeamName = false;\n".format(analysis_name))
    JSmod.append("{:}.step(1).regenerateMeshOption = anConditionalRegenerateMesh;\n".format(analysis_name))
    #
    JSmod.append("//\n")
    JSmod.append("{:}.add(LinearAnalysis());\n".format(analysis_name)) 
    JSmod.append("{:}.add(LoadResultsActivity());\n".format(analysis_name))
    JSmod.append("//\n")
    return JSmod
#
#
def print_wave_sets(_metocean, factors, seastate_name):
    """
    """
    #
    seastate_name = seastate_name.strip()
    #
    #  Units
    _length_factor = factors[0]
    #
    header = 'Regular Wave Sets'
    JSmod = printing.section_head(header, comment_prefix='//')    
    #
    i = 0
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        _wave = _seastate.wave
        #
        if 'calm' in _wave.theory.lower():
            continue
        #
        if i == 0:
            JSmod.append("\n")
            JSmod.append("Wave_{:} = RegularWaveSet();\n"
                         .format(seastate_name))
        #
        JSmod.append("// Load Name : {:} Number : {:}\n"
                     .format(_wave.name, _wave.number))
        
        JSmod.append("Wave_{:}.add(RegularWave"
                     .format(seastate_name))
        
        JSmod.append("({:} deg, {:} m, WavePeriod({:} s), {:} deg));\n"
                     .format(_seastate.wave_direction,
                             _wave.height * _length_factor,
                             _wave.period, _wave.phase))
        
        i+= 1
    #
    JSmod.append("//\n")
    return JSmod
#
def print_current_profiles(_metocean, factors, seastate_name):
    """
    """
    #
    seastate_name = seastate_name.strip()
    #    
    #  Units
    _length_factor = factors[0]
    #
    header = 'Current Profiles'
    JSmod = printing.section_head(header, comment_prefix='//')    
    #
    _list_current = []
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        if _seastate.current:
            _curr = _seastate.current
            
            if _curr.name in _list_current:
                continue
            else:
                _list_current.append(_curr.name)
        else:
            continue
        #
        # Elevations
        #
        JSmod.append("{:}_elevations = Array("
                     .format(_curr.name + "_" + seastate_name))
        
        _range = len(_curr.profile)
        i = 0
        for _elev in _curr.profile:
            i += 1
            JSmod.append("{:} m".format(_elev[0] * _length_factor))
            if i < _range:
                JSmod.append(", ")
        #
        JSmod.append(");\n")
        #       
        #
        # Elevations
        #
        JSmod.append("{:}_velocities = Array("
                     .format(_curr.name + "_" + seastate_name))
        
        i = 0
        for _elev in _curr.profile:
            i += 1
            JSmod.append("{:} m/s".format(_elev[1]))
            if i < _range:
                JSmod.append(", ")
        #
        JSmod.append(");\n")
    #
    JSmod.append("\n")
    JSmod.append("\n")
    #
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        if _seastate.current:
            _curr = _seastate.current
        else:
            continue
        #
        _range = len(_curr.profile)
        #
        # Angles
        #
        JSmod.append("Current_{:}_{:}_directions = Array("
                     .format(_seastate.number, seastate_name))        
        i = 0
        for _elev in _curr.profile:
            i += 1
            JSmod.append("{:} deg".format(_seastate.current_direction))
            if i < _range:
                JSmod.append(", ")
        #
        JSmod.append(");\n") 
        #
        #
        JSmod.append("Current_{:}_{:} = CurrentProfileRelDir("
                     .format(_seastate.number, seastate_name))
        JSmod.append("{:}_elevations, "
                     .format(_curr.name + "_" + seastate_name))
        JSmod.append("Current_{:}_{:}_directions, "
                     .format(_seastate.number, seastate_name))
        
        _rel = 'false'
        if _curr.absolute_elevation:
            _rel = 'true'
        JSmod.append("{:}_velocities, dtRelativeX, {:});\n"
                     .format(_curr.name + "_" + seastate_name, _rel))
        
        JSmod.append("\n")
    #
    JSmod.append("//\n")
    #
    return JSmod
#
def get_heading(alignment):
    """
    """
    if alignment == 'wave':
        return "dtRelativeHeading, "
    
    elif alignment == 'along':
        return "dtAlongHeading, "
    
    else:
        return "dtRelativeX, "
#
def print_wind_profiles(_metocean, factors, seastate_name):
    """
    """
    #
    seastate_name = seastate_name.strip()
    #    
    #  Units
    _length_factor = factors[0]
    #
    header = 'Wind Profiles'
    JSmod = printing.section_head(header, comment_prefix='//')
    #
    for key in sorted(_metocean, key = lambda name: _metocean[name].number):
        _seastate = _metocean[key]
        if _seastate.wind:
            _wind = _seastate.wind
        else:
            continue
        
        JSmod.append("// load case : {:} \n".format(_wind.name))
        
        JSmod.append("Wind_{:}_{:} = WindProfileRelDir("
                     .format(_seastate.number, seastate_name))
        
        JSmod.append("{:} m/s, {:} m, "
                     .format(_wind.velocity * _length_factor,
                             _wind.height* _length_factor))
        
        #
        if 'general' in _wind.formula.lower():
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("{:}, wpGeneral, {:} deg, {:}"
                         .format(_wind.power,
                                 _seastate.wind_direction, 
                                 _wind.gust_factor))
        
        elif 'normal' in _wind.formula.lower():
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("wpNormal, {:} deg, {:}"
                         .format(_seastate.wind_direction, 
                                 _wind.period_ratio))
        
        elif 'abs' in _wind.formula.lower():
            JSmod.append("{:}, ".format(_wind.gust_factor))
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("wpABS, {:} deg, {:}"
                         .format(_seastate.wind_direction, 
                                 _wind.period_ratio))
        
        elif _wind.formula.lower() == 'extreme':
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("wpExtreme, {:} deg, {:}"
                         .format(_seastate.wind_direction, 
                                 _wind.period_ratio))
        
        elif 'api21' in _wind.formula.lower():
            JSmod.append(get_heading(_wind.alignment))
            JSmod.append("wpExtremeAPI21, {:} deg, {:}"
                         .format(_seastate.wind_direction, 
                                 _wind.period_ratio))
        
        if _seastate.wind_areas:
            JSmod.append(", wfProjectedPressure);\n")
        else:
            JSmod.append(");\n")
    #
    JSmod.append("//\n")
    return JSmod
#
#
# 
# Copyright (c) 2009-2017 fem2ufo
# 


# Python stdlib imports
import math

# package imports
import locmatrix.process.printing as printing
#import locmatrix.process.operations as operations

#
def check_spring(_spr, spr_type):
    """
    """
    #
    _force = _spr.force
    _disp = _spr.displacement
    # Qz
    if 'qz' in spr_type.lower():
        if _force[1] == 0.0 and _disp[1] == 0.0:
            f_add = [0 for x in range(len(_force))]
            d_add = [item * -1 for item in _disp[::-1]]
        
            _force = f_add[:len(_force)-2]
            _force.extend(_spr.force[1:])
            _spr.force = _force
        
            _disp = d_add[:len(_disp)-2]
            _disp.extend(_spr.displacement[1:])
            _spr.displacement = _disp
    # Py & Tz
    else:
        if _force[0] == 0.0 and _disp[0] == 0.0 :
            f_add = [item * -1 for item in _force[::-1]]
            d_add = [item * -1 for item in _disp[::-1]]
            
            _force = f_add[:len(_force)-1]
            _force.extend(_spr.force)
            _spr.force = _force
        
            _disp = d_add[:len(_disp)-1]
            _disp.extend(_spr.displacement)
            _spr.displacement = _disp
    #
    return _spr
#
#
#
# Foundation
#
def print_pytzqz(foundation, factors):
    """
    """
    header = 'FOUNDATIONS'
    JSmod = printing.head_line(header, comment_prefix='//')
    _section = 'Soil Data and Soil Curves'
    JSmod.extend(printing.section_head(_section, comment_prefix='//'))    
    JSmod.append("Soil_default = Sand(false, 1, 2000 Kg/m^3, 30 deg);\n")
    JSmod.append("Soil_default.frictionRatio = 0;\n")
    JSmod.append("Soil_default.zDisplacement = 1 m;\n")
    JSmod.append("SoilClay = Clay(false, 1, 1927.77 Kg/m^3, 0.5, 200000 Pa, 0 m, 200000 Pa, -100 m);\n")
    JSmod.append("//\n")
    JSmod.append("//\n")    
    #
    for _key1, _soil in sorted(foundation.items()):
        _layer_no = 0
        for key in sorted(_soil.layers, key = lambda name: _soil.layers[name].depth):
            _layer = _soil.layers[key]        
            _layer_no += 1
            JSmod.append("//\n")
            JSmod.append("{:}_{:} = SoilCurves(pyManual, tzManual, qzManual);\n"
                         .format(_soil.name, _layer_no))             
            
            JSmod.extend(print_spring_data(_soil.name, _layer_no, _soil.diameter, 
                                           _layer.curve, factors))
        JSmod.append("//\n")
    #
    JSmod.append("//\n")
    JSmod.append("SoilData_1 = SoilData(0 Pa, 0.5, 0 Pa, 0 Pa, 0, 0 Pa, 0);\n")
    JSmod.append("//\n")
    #
    return JSmod
#
def get_spring(_spring, _spr_type,
               unit_stress_factor=1,
               length_factor=1):
    """
    """
    _force = _spring.force
    _disp = _spring.displacement    
    _steps = len(_force)
    #
    # displacement
    #
    JSmod = []
    JSmod.append("Array(")
    
    if "qz" in _spr_type.lower():
        JSmod.append("{:1.4e} m, "
                     .format(-1.0))
        JSmod.append("{:1.4e} m, ".format(0))
    else:
        JSmod.append("{:1.4e} m, "
                     .format(0))
    #
    _top = 0
    for x in range(_steps - 1):
        
        if _force[x] <= 0.0 and _disp[x] <= 0.0 :
            continue
        
        if _top == _disp[x]:
            continue
        
        JSmod.append("{:1.4e} m, "
                     .format(_disp[x] * length_factor))
        
        _top = _disp[x]
    # end
    if _top != _disp[_steps-1]:
        JSmod.append("{:1.4e} m), "
                     .format(_disp[_steps-1] * length_factor))
    else:
        JSmod.append("), ")
    #
    # Stress
    #
    JSmod.append("Array(")
    JSmod.append("{:1.4e} Pa, ".format(0))
    
    if "qz" in _spr_type.lower():
        JSmod.append("{:1.4e} Pa, ".format(0))
    #
    _top = 0
    for x in range(_steps-1):
        if _force[x] <= 0.0 and _disp[x] <= 0.0 :
            continue
        
        if _top == _disp[x]:
            continue        
        
        JSmod.append("{:1.4e} Pa, "
                     .format(_force[x] * unit_stress_factor))
        _top = _disp[x]
    #
    if _top != _disp[_steps-1]:
        JSmod.append("{:1.4e} Pa));\n"
                     .format(_force[_steps-1] * unit_stress_factor))
    else:
        JSmod.append("));\n")    
    #
    return JSmod
#
#
def get_springTZ(_spring, unit_stress_factor=1,
                 length_factor=1):
    """
    """
    #
    _force = _spring.force
    _disp = _spring.displacement    
    _steps = len(_force)
    #
    # displacement
    #
    JSmod = []
    JSmod.append("Array(")
    #
    _top = 0
    for x in range(_steps - 1):
        if _top == _disp[x]:
            continue
        JSmod.append("{:1.4e} m, "
                     .format(_disp[x] * length_factor))
        _top = _disp[x]
    # end
    if _top != _disp[_steps-1]:
        JSmod.append("{:1.4e} m), "
                     .format(_disp[_steps-1] * length_factor))
    else:
        JSmod.append("), ")
    #
    # Stress
    #
    JSmod.append("Array(")
    #
    _top = 0
    for x in range(_steps-1):
        if _top == _disp[x]:
            continue        
        JSmod.append("{:1.4e} Pa, "
                     .format(_force[x] * unit_stress_factor))
        _top = _disp[x]
    # end
    if _top != _disp[_steps-1]:
        JSmod.append("{:1.4e} Pa));\n"
                     .format(_force[_steps-1] * unit_stress_factor))
    else:
        JSmod.append("));\n")    
    #
    return JSmod
#
#
def print_spring_data(_soil_name, _layer_no, _diameter, _layer, factors):
    """
    """
    # default force units is meter*second^-2*gram
    # to convert to meter*second^-2*kilogram : 
    if factors[4] == 1:
        _force_factor = 1
    else:
        _force_factor = factors[4]  / 1000.0
    # need to fix gravity units
    _gravity_unit = factors[0]
    _length_factor = factors[0]
    #
    JSmod = []
    #
    # PY
    #
    _spr_type = 'py'
    _spring = check_spring(_layer['PY'], _spr_type)
    
    unit_stress_factor = _force_factor / (_diameter * _length_factor)
    
    JSmod.append("{:}_{:}.addManualPY({:} m, "
                 .format(_soil_name, _layer_no, 
                         _diameter * _length_factor))
    #
    JSmod.extend(get_spring(_spring, _spr_type,
                            unit_stress_factor,
                            _length_factor))
    #
    # TZ
    #
    _spring = check_spring(_layer['TZ'], 'tz')
    
    unit_stress_factor = _force_factor / (_diameter * _length_factor * math.pi)
    
    JSmod.append("{:}_{:}.addManualTZ({:} m, "
                 .format(_soil_name, _layer_no, 
                         _diameter * _length_factor))
    #
    JSmod.extend(get_springTZ(_spring, 
                              unit_stress_factor,
                              _length_factor))
    #
    # QZ
    #
    try:
        _spr_type = 'qz'
        _spring = check_spring(_layer['QZ'], _spr_type)
        
        unit_stress_factor = (4.0 * _force_factor) / ((_diameter * _length_factor)**2 * math.pi)
        
        JSmod.append("{:}_{:}.addManualQZ({:} m, "
                     .format(_soil_name, _layer_no, 
                             _diameter * _length_factor))
        #
        JSmod.extend(get_spring(_spring, _spr_type,
                                unit_stress_factor,
                                _length_factor))
    except KeyError:
        JSmod.append("{:}_{:}.addManualQZ({:} m, Array(0 m, 0.01 m), Array(0 Pa, 0 Pa));\n"
                     .format(_soil_name, _layer_no, 
                             _diameter * _length_factor))
    #
    return JSmod
#
#
#
    

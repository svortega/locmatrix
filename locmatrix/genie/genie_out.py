#
# Python stdlib imports
#import os

# package imports
import locmatrix.xl.read_xl as xlread
import locmatrix.genie.loading as js_load
import locmatrix.genie.seastate as seastate
import locmatrix.genie.foundations as foundations
import locmatrix.process.printing as printing
#
#
#
def print_foundation(soil, factors, soil_name):
    """
    """
    js_soil = []
    js_soil.extend(foundations.print_pytzqz(soil, factors))
    file_out = soil_name + '_springs.js'
    add_out = open(file_out, 'w+')
    add_out.write("".join(js_soil))
    add_out.close()
#
#
def print_metocean(analysis, analysis_case, 
                   design_condition, factors):
    """
    """
    #
    # print locations + sea state load combination
    #
    top_load_combinations = {}
    #
    #for _name, _analysis in analysis.items():
    #
    js_metocean = []
    #
    #
    if analysis_case[1]:
        # wind 
        try:
            js_metocean.extend(seastate.print_wind_profiles(analysis.metocean_combination,
                                                            factors,
                                                            seastate_name=analysis_case[1]))
            
            js_metocean.extend(js_load.print_equipment_wind_areas(analysis.metocean_combination,
                                                                  seastate_name=analysis_case[1],
                                                                  design_condition=design_condition))
        except AttributeError:
            pass
        # current
        try:
            js_metocean.extend(seastate.print_current_profiles(analysis.metocean_combination,
                                                               factors,
                                                               seastate_name=analysis_case[1]))
        except AttributeError:
            pass
        # wave sets
        try:
            js_metocean.extend(seastate.print_wave_sets(analysis.metocean_combination,
                                                        factors, 
                                                        seastate_name=analysis_case[1]))
        except AttributeError:
            pass                    
        #
        #
        js_metocean.extend(seastate.print_conditions(analysis.metocean_combination,
                                                     analysis_case))
        #
    else:
        js_metocean.extend(seastate.print_no_conditions(analysis_case[0]))
    #
    #
    return js_metocean
#
def print_location(soil, analysis, metocean,
                   mudline, surface, factors,
                   js_meto):
    """
    """
    # 
    js_location = []
    analysis_name = 'Location_' + analysis
    #
    # foundation
    if metocean:
        if soil:
            js_location.extend(seastate.print_enviroment_soil(surface, 
                                                              mudline,
                                                              soil, factors,
                                                              location_name=analysis_name))
        else:
            js_location.extend(seastate.print_enviroment_no_soil(surface, 
                                                                 mudline,
                                                                 factors,
                                                                 location_name=analysis_name))
    else:
        if soil:
            js_location.extend(seastate.print_soil_no_enviroment(soil, 
                                                                 mudline,
                                                                 factors,
                                                                 location_name=analysis_name))
        else:
            return
    
    fileJS = '1_' + analysis_name +'.js'
    JSmodel = open(fileJS, 'w+')
    JSmodel.write("".join(js_location))
    JSmodel.write("".join(js_meto))
    JSmodel.write("".join(printing.EOF(comment_prefix='//')))
    JSmodel.close()
#
def print_analysis(metocean_combination, analysis_case, 
                   mudline, minimum_depth, water_depths):
    """
    """
    #
    js_analysis = []
    #
    # Print analysis
    if metocean_combination:
        js_analysis.extend(seastate.print_analysis_metocean(metocean_combination, 
                                                            water_depths,
                                                            mudline,
                                                            minimum_depth,
                                                            analysis_case))
    else:
        js_analysis.extend(seastate.print_analysis_no_metocean(analysis_case[0]))
    #
    # print file
    #
    fileJS = '2_' + analysis_case[0] + '.js'
    JSmodel = open(fileJS, 'w+')
    JSmodel.write("".join(js_analysis))
    JSmodel.write("".join(printing.EOF(comment_prefix='//')))
    JSmodel.close()
    #print('    * File : {:}'.format(fileJS))
    #    
#
def print_js(factored_sheet, actual_sheet, 
             metocean_sheet, soil_sheet,
             analysis_case, design_condition, 
             mudline, water_depth):
    """
    """
    #
    surface = mudline + water_depth
    factors = [1, 1, 1, 1, 1, 1]
    #
    if soil_sheet[0]:
        foundation, springs = xlread.get_foundation(soil_sheet[0])
        if soil_sheet[1]:
            print_foundation(foundation, factors, soil_sheet[0])
    else:
        foundation = None
        analysis_case[2] = None
    #
    js_meto = []
    if metocean_sheet[0] :
        metocean =  xlread.get_metocean(metocean_sheet[0])
        name = analysis_case[1]
        analysis = metocean[name]
        metocean_combination = analysis.metocean_combination
        water_depths = analysis.condition.water_depth
        
        if metocean_sheet[1]:
            js_meto = print_metocean(analysis, analysis_case, 
                                     design_condition[0], factors)
    else:
        metocean_combination = None
        analysis_case[1] = None
        water_depths = [water_depth]
    #
    #
    print_location(foundation, analysis_case[0], metocean_sheet[0],
                   mudline, surface, factors, js_meto)    
    #
    print_analysis(metocean_combination, 
                   analysis_case, 
                   mudline, 
                   water_depth,
                   water_depths)
    #
    # Print Load Combinations
    #
    _name = analysis_case[0]
    js_file = []
    #
    level_1, functional = xlread.get_basic_combinations(actual_sheet[0], 
                                                        metocean_combination,
                                                        design_condition[0])
    #
    js_file.extend(js_load.print_load_combinations(level_1, 
                                                   functional, _name))
    #
    js_file.extend(js_load.print_load_combinations_seastate(level_1, 
                                                            functional, _name))
    #
    if actual_sheet[1]:
        file_out = '3_' + str(actual_sheet[0]) + '.js'
        add_out = open(file_out, 'w+')
        add_out.write("".join(js_file))
        add_out.close()
    #
    if factored_sheet[1]:
        #
        js_file = []
        #
        level_2, level_3 = xlread.get_factored_combinations(factored_sheet[0], 
                                                            design_condition[0])
        #
        #
        js_file.extend(js_load.print_top_load_combinations(level_2, level_1, 
                                                           _name))      
        #
        file_out = '4_' + str(factored_sheet[0]) + '.js'
        add_out = open(file_out, 'w+')
        add_out.write("".join(js_file))
        add_out.close()
        #
    #
    #
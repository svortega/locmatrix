# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports
#import math

# package imports
import locmatrix.process.printing as printing
#import locmatrix.process.operations as operations

#
#
#
#
# First level load combination (LCA)
#
def print_load_combinations(combination, functional, 
                            analysis_name, 
                            sub=None, level=1):
    """
    """
    if sub:
        sub = str(sub) + "_"
    else:
        sub = ""

    header = 'LOAD COMBINATION'
    JSmod = printing.head_line(header, comment_prefix='//')
    
    header = 'Load Combination Adjusted (LCA) - Level: {:}'.format(level)
    JSmod.extend(printing.section_head(header, comment_prefix='//'))
    
    #
    _load_name = [key for key in sorted(combination, key = lambda name: combination[name].number)]
    #
    for _name in _load_name:
        _lc = combination[_name]    
        #
        if _lc.design_condition == 'storm':
            _case = 'lcStorm'

        elif _lc.design_condition == 'earthquake':
            _case ='lcEarthquake'

        elif _lc.design_condition == 'harbour':
            _case ='lcHarbour'

        elif _lc.design_condition == 'seagoing':
            _case ='lcSeagoing'

        else:
            _case ='lcOperating'
        #
        # print load combination
        #
        JSmod.append('{:}{:} = LoadCombination({:});\n'
                     .format(sub, _lc.name, analysis_name))

        JSmod.append('{:}{:}.setFemLoadcase({:});\n'
                     .format(sub, _lc.name, int(float(_lc.number))))

        JSmod.append('{:}{:}.designCondition({:});\n'
                     .format(sub,  _lc.name, _case))

        JSmod.append('\n')
    #
    #
    header = 'Load Combination Adjusted : Basic Load Combination'
    JSmod.extend(printing.section_head(header, comment_prefix='//'))    
    #
    for _name in _load_name:
        _lc = combination[_name]
        _flag = False
        for _item in _lc.functional_load:
            _flag = True
            _load_name = functional[_item[0]].name
            JSmod.append('{:}{:}.addCase({:}{:}, {:});\n'
                         .format(sub, _lc.name, 
                                 sub, _load_name, _item[1]))

        if _flag:
            JSmod.append('\n')
    #
    JSmod.append('// \n')
    #
    return JSmod
#
def print_load_combinations_seastate(combination, functional, 
                                     analysis_name, level=1):
    """
    """
    #
    header = 'Load Combination Adjusted : Metocean Combinations'
    JSmod = printing.section_head(header, comment_prefix='//')
    #
    _load_name = [key for key in sorted(combination, key = lambda name: combination[name].number)]
    #
    for _name in _load_name:
        _lc = combination[_name]
        if _lc.metocean_load:
            for _item in _lc.metocean_load:
                _seastate = _item[0]
                _factor = _item[1]
                JSmod.append('{:}.addCase({:}.WLC({:}, 1), {:}); //'
                             .format(_lc.name, analysis_name, 
                                     _seastate.number, _factor))
                
                JSmod.append(' {:}, Number {:}\n'.format(_seastate.name, 
                                                         _seastate.number))                
            # check wind areas
            #try:
            if _seastate.wind_areas:
                _wind = _seastate.wind
                #_new_name = 'WindArea_' + _seastate.name
                _new_name = _wind.name
                JSmod.append('{:}.addCase({:}, {:});\n'
                             .format(_lc.name, 
                                     _new_name, _factor))
            #except AttributeError:
            #    pass
            #
            JSmod.append('\n')
            #JSmod.append('// \n')
    #
    JSmod.append('// \n')
    return JSmod
#
# Second level load combation (LCF)
#
def print_top_load_combinations(combination, basic_lc, 
                                analysis_name, 
                                sub=None, level=2):
    """
    """
    if sub:
        sub = str(sub) + "_"
    else:
        sub = ""

    header = 'LOAD COMBINATIONS'
    JSmod = printing.head_line(header, comment_prefix='//')
    
    header = 'Load Combination Factored (LCF) - Level: {:}\n'.format(level)
    JSmod.extend(printing.section_head(header, comment_prefix='//'))    
    #
    _load_name = [key for key in sorted(combination, key = lambda name: combination[name].number)]
    #
    for _name in _load_name:
        _lc = combination[_name]
        #
        if _lc.design_condition == 'storm':
            _case = 'lcStorm'
        
        elif _lc.design_condition == 'earthquake':
            _case ='lcEarthquake'
        
        elif _lc.design_condition == 'harbour':
            _case ='lcHarbour'
        
        elif _lc.design_condition == 'seagoing':
            _case ='lcSeagoing'
        
        else:
            _case ='lcOperating'
        #
        #
        JSmod.append('{:} = LoadCombination({:});\n'
                     .format( _lc.name, analysis_name))
        
        JSmod.append('{:}{:}.setFemLoadcase({:});\n'
                     .format(sub, _lc.name, int(float(_lc.number))))
        
        JSmod.append('{:}.designCondition({:});\n'
                     .format(_lc.name, _case))
        
        JSmod.append('\n')
    #
    #
    header = 'Load Combination Factored : Adjusted Combinations'
    JSmod.extend(printing.section_head(header, comment_prefix='//'))    
    #
    for _name in _load_name:
        _lc = combination[_name]
        _flag = False
        for _item in _lc.functional_load:
            _flag = True
            _factor = _item[1]
            _load_number = basic_lc[_item[0]].number
            _load_name = basic_lc[_item[0]].name
            JSmod.append('{:}.addCase({:}{:}, {:});\n'
                         .format(_lc.name,  
                                 sub, _load_name, _factor))
        
        if _flag:
            JSmod.append('\n')
    #
    JSmod.append('// \n')
    return JSmod
#
def print_top_combinations_seastateXX(combination, basic_lc, 
                                    analysis_name, 
                                    sub=None, level=2):
    """
    """
    #
    header = 'Factored Load Combination : Metocean'
    JSmod.extend(printing.section_head(header, comment_prefix='//'))    
    #
    for _lc in combination.values():
        
        if _lc.metocean_load:
            
            for _item in _blc.metocean_load:
                _wave = _item[0]
                _factor = _item[1]
                JSmod.append('{:}.addCase({:}.WLC({:}, 1), {:});\n'
                             .format(_lc.name, analysis_name, 
                                     _wave.number, _factor))                
    #
    JSmod.append('// \n')
    return JSmod
#
#
# Equipment section
#
def print_basic_equipment_loading(load, factors, sub=None):
    """
    """
    if sub:
        sub = str(sub) + "_"
    else:
        sub = ""
    #
    _length_factor = factors[0]
    #
    header = 'EQUIPMENT LOAD MODELLING'
    JSmod = printing.head_line(header, comment_prefix='//')
    _load_name = [key for key in sorted(load, key = lambda name: load[name].number)]
    #
    for key in _load_name:
        _load = load[key]
        for _equipment in _load.areas:
            #for _node in _equipment.nodes:
            JSmod.append('{:}{:}.placeAtPoint({:},'
                         .format(sub, _load.name, _equipment.name))
            
            #x1, y1, z1 = _node.get_coordinates(_length_factor)
            mass, x1, y1, z1 = _equipment.mass
            JSmod.append(' Point({: 1.3f} m, {: 1.3f} m, {: 1.3f} m), '
                         .format(x1, y1, z1))
            # x direction
            if _equipment.wind_pressure[0]:
                JSmod.append('LocalSystem(Vector3d(0 m,1 m,0 m), Vector3d(1 m,0 m,0 m)));\n')
            # y direction
            if _equipment.wind_pressure[1]:
                JSmod.append('LocalSystem(Vector3d(1 m,0 m,0 m), Vector3d(0 m,-1 m,0 m)));\n')
            #
            JSmod.append('{:}{:}.constantLoad({:});\n'
                                 .format(sub, _load.name, _equipment.name))
            # x direction
            if _equipment.wind_pressure[0]:
                JSmod.append('{:}{:}.equipment({:}).'
                             .format(sub, _load.name, _equipment.name))
                JSmod.append('sideInLocalSystem(Vector3d(0 m,0 m,1 m)).intensity = ')
                JSmod.append('ConstantWindPressureLoadCase({:}, {:});\n'
                             .format(_equipment.wind_pressure[0].drag_coefficient,
                                     _equipment.wind_pressure[0].suction_factor))
                
                JSmod.append('{:}{:}.equipment({:}).'
                                         .format(sub, _load.name, _equipment.name))
                JSmod.append('sideInLocalSystem(Vector3d(0 m,0 m,-1 m)).intensity = ')
                JSmod.append('ConstantWindPressureLoadCase({:}, {:});\n'
                                         .format(_equipment.wind_pressure[0].drag_coefficient,
                                                 _equipment.wind_pressure[0].suction_factor))
            # y direction
            if _equipment.wind_pressure[1]:
                JSmod.append('{:}{:}.equipment({:}).'
                             .format(sub, _load.name, _equipment.name))
                JSmod.append('sideInLocalSystem(Vector3d(0 m,0 m,1 m)).intensity = ')
                JSmod.append('ConstantWindPressureLoadCase({:}, {:});\n'
                             .format(_equipment.wind_pressure[1].drag_coefficient,
                                     _equipment.wind_pressure[1].suction_factor))
                
                JSmod.append('{:}{:}.equipment({:}).'
                                         .format(sub, _load.name, _equipment.name))
                JSmod.append('sideInLocalSystem(Vector3d(0 m,0 m,-1 m)).intensity = ')
                JSmod.append('ConstantWindPressureLoadCase({:}, {:});\n'
                                         .format(_equipment.wind_pressure[1].drag_coefficient,
                                                 _equipment.wind_pressure[1].suction_factor))
            JSmod.append('\n')
    #
    JSmod.append('//\n')
    return JSmod
#
def print_equipment_wind_areas(load, seastate_name,
                               design_condition,
                               sub=None):
    """
    """
    if sub:
        sub = str(sub) + "_"
    else:
        sub = ""
    #
    seastate_name = seastate_name.strip()
    #
    header = 'Equipment Wind Areas'
    JSmod = printing.section_head(header, comment_prefix='//')
    #_load_name = [key for key in sorted(load, key = lambda name: load[name].wind.number)]
    #
    if design_condition == 'storm':
        _case = 'lcStorm'
    elif design_condition == 'earthquake':
        _case ='lcEarthquake'
    elif design_condition == 'harbour':
        _case ='lcHarbour'
    elif design_condition == 'seagoing':
        _case ='lcSeagoing'
    else:
        _case ='lcOperating'
    #
    #
    for key, _load in load.items():
        if not _load.wind:
            continue
        #_load = load[key]
        _new_name = _load.wind.name # 'WindArea_' + _load.name
        
        if _load.wind_areas:
            JSmod.append('\n')
            #JSmod.append('{:}{:} = LoadCase();\n'.format(sub, _new_name))
            #JSmod.append('{:}{:}.setFemLoadcase({:});\n'.format(sub, _new_name))
            #JSmod.append('{:}{:}.designCondition({:});\n'.format(sub, _new_name, _case))
            #
            #JSmod.append('{:}{:}.meshLoadsAsMass(false);\n'
            #             .format(sub, _new_name))

            JSmod.append('{:}{:}.setWindField(1.226 Kg/m^3, 0.010 m, 0 deg, Wind_{:}_{:});\n'
                         .format(sub, _new_name, _load.number, seastate_name))
            # needs to check if the case
            JSmod.append('{:}{:}.excludeSelfWeight();\n'
                         .format(sub, _new_name))

            JSmod.append('{:}{:}.includeStructureMassWithRotationField();\n'
                         .format(sub, _new_name))
            JSmod.append('//\n')
            JSmod.append('//\n')
    #
    JSmod.append('//\n')
    return JSmod
# 
#
def print_equipments(equipments,
                     factors, subfix=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    #
    # convert from gram to kilogram
    _mass_factor = factors[1] * 1000.0     
    #
    header = 'EQUIPMENTS'
    JSmod = printing.head_line(header, comment_prefix='//')
    
    #header = 'Equipments'
    #JSmod.extend(printing.section_head(header, comment_prefix='//'))

    for key, _equipment in sorted(equipments.items()):
        JSmod.append('{:}{:} = PrismEquipment('.format(sub, _equipment.name))
        x1, y1, z1 = _equipment.size
        _mass = 0
        if _equipment.mass:
            _mass, CoGx, CoGy, CoGz = _equipment.mass

        JSmod.append('{: 1.3f} m, {: 1.3f} m, {: 1.3f} m, {:1.4e} kg);\n'
                     .format(x1, y1, z1, _mass * _mass_factor))        
    #
    JSmod.append('// \n')
    return JSmod
#
def print_load_interface(equipments, # joints,
                         factors, subfix=False):
    """
    """
    sub = ""
    if subfix:
        sub = str(subfix) + "_"
    #
    _length_factor = factors[0]
    #
    header = 'LOAD INTERFACES'
    JSmod = printing.head_line(header, comment_prefix='//')

    #header = 'Load interfaces'
    #JSmod.extend(printing.section_head(header, comment_prefix='//'))    

    for key, _equipment in sorted(equipments.items()):
        JSmod.append('{:}{:}_LI = LoadInterface();\n'
                     .format(sub, _equipment.name))

        _load_interface = list(set(_equipment.load_interface))
        for _basic in sorted(_load_interface):
            JSmod.append('{:}{:}_LI.addEquipment({:}, {:});\n'
                         .format(sub, _equipment.name, 
                                     _equipment.name, _basic))

        for _joint in _equipment.joints:
            _name = 'dum' + str(_joint.name) + '_int'
            JSmod.append('{:}{:}_LI.add({:});\n'
                         .format(sub, _equipment.name, 
                                 _name))

        JSmod.append('\n')
    #
    JSmod.append('//\n')
    return JSmod

#
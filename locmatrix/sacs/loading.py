# 
# Copyright (c) 2009-2017 fem2ufo
# 


#
# Python stdlib imports

# package imports
import locmatrix.process.printing as printing
#import locmatrix.process.operations as operations



# --------------------------------------------------------------------
# Loading control
#
#
def print_load_selection(combination):
    """
    """
    _load_name = [key for key in sorted(combination, key = lambda name: combination[name].number)
                  if combination[key].functional_load or combination[key].metocean_load]
    
    _function = 'ST'
    JSmod =[]
    JSmod.append('LCSEL {:2s}{:>8.8s}'.format(_function, " "))
    _step = 12
    for x, _name in enumerate(_load_name):
        _lc = combination[_name]
        JSmod.append('{:>4.4s} '.format(str(_lc.number)))
        if x == _step:
            _step *= 2
            JSmod.append('\n')
            JSmod.append('LCSEL {:2s}{:>10.10s}'.format(_function, " "))
    JSmod.append('\n')
    return JSmod
#
def print_combination_start():
    """
    """
    header = 'LOAD COMBINATIONS'
    JSmod = printing.head_line(header, comment_prefix='*')    
    JSmod.append('*\n')
    JSmod.append('LCOMB\n')
    return JSmod
#
def print_load_combination(combination, functional, 
                           analysis_name, level=1):
    """
    """
    #
    header = 'LOAD COMBINATION LEVEL {:}'.format(level)
    JSmod = printing.section_head(header, comment_prefix='*')
    #
    _load_name = [key for key in sorted(combination, key = lambda name: combination[name].number)
                  if combination[key].functional_load or combination[key].metocean_load]
    #
    for _name in _load_name:
        _lc = combination[_name]
        JSmod.append('* {:}\n'.format(_lc.name))
        _step = 5
        if _lc.functional_load:
            JSmod.append("LCOMB {:>4.4s} ".format(str(_lc.number)))
            for x, _functional in enumerate(_lc.functional_load):
                _basic = functional[_functional[0]]
                JSmod.append('{:>4.4s}{:>6.6s}'.format(str(_basic.number),
                                                       str(_functional[1])))
                if x == _step:
                    _step *= 2
                    JSmod.append('\n')
                    JSmod.append("LCOMB {:>4.4s} ".format(str(_lc.number)))
                # sacs number of combination limitation
                if x > 47:
                    JSmod.append('*')
            JSmod.append('\n')
        #
        _step = 5
        if _lc.metocean_load:
            JSmod.append("LCOMB {:>4.4s} ".format(str(_lc.number)))
            for x, _functional in enumerate(_lc.metocean_load):
                _meto = _functional[0]
                JSmod.append('{:>4.4s}{:>6.6s}'.format(str(_meto.number),
                                                       str(_functional[1])))
                if x == _step:
                    _step *= 2
                    JSmod.append('\n')
                    JSmod.append("LCOMB {:>4.4s} ".format(str(_lc.number)))
                # sacs number of combination limitation
                if x > 47:
                    JSmod.append('*')
            JSmod.append('\n')
    #
    #
    JSmod.append('*\n')
    return JSmod
#
#
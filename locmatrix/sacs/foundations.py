# 
# Copyright (c) 2009-2017 fem2ufo
# 


# Python stdlib imports
import math

# package imports
import locmatrix.process.printing as printing

#
# --------------------------------------------------------------------
#
#
def print_analysis_parameters(material_density='8.25'):
    """
    """
    #
    _unit='MN'
    #
    header = 'FOUNDATIONS'
    JSmod = printing.head_line(header, comment_prefix='*')
    # units option
    JSmod.append("PSIOPT +Z{:>3.3s}{:>6.6s}".format(_unit, " "))
    # analysis options
    _pile_analysis = 'SM'
    JSmod.append("{:>2.2s}{:>2.2s}{:>2.2s}"
                 .format(" ", " ", _pile_analysis))
    # convergence criteria
    _displacement = '0.00254'
    _rotation = '0.0001'
    _iterations = '20'
    JSmod.append("{:>8.8s}{:>8.8s}{:>3.3s}"
                 .format(_displacement, _rotation, _iterations))
    # output options
    _conv = '0.5'
    JSmod.append("PT{:>16.16s}100  {:>6.6s}{:>8.8s}\n"
                 .format(" ", _conv, material_density))
    #
    JSmod.append("*\n")
    return JSmod
#
#
def print_pytzqz(foundation, factors):
    """
    """
    #
    # FIXME:
    # default force units is meter*second^-2*gram
    # to convert to meter*second^-2*kilogram : 
    # force unit fixed to kN
    if factors[4] == 1:
        _force_factor = factors[4] / 1000.0
    else:
        _force_factor = factors[4] / 1.0e6
    # FIXME: need to fix gravity units
    _gravity_unit = factors[0]
    # FIXME : length unit fixed to cm
    _length_factor = factors[0] * 100.0
    #    
    JSmod = []
    #
    # P-Y
    for _key1, _soil in sorted(foundation.items()):
        _diameter = _soil.diameter * _length_factor
        _layer_top = 0
        JSmod.append("*\n")
        JSmod.append("* Soil Lateral Resistance, Name: {:}, Number: {:}\n"
                             .format(_soil.name, _soil.number))
        JSmod.append("*\n")
        # SOIL LATERAL HEADER LINE
        #
        # 18-20 NUMBER OF SOIL STRATA
        _strata_number = str(len(_soil.layers))
        JSmod.append("SOIL LATERAL HEAD{:>3s} ".format(_strata_number))
        # 22-23 MORE THAN 30 DATA POINTS FOR P-Y CURVE
        # 24-27 P-Y CURVE SCALING
        _steps = max([len(_layer.curve['PY'].force) for _layer in _soil.layers.values()])
        if _steps > 30:
            JSmod.append("{:>2s}YEXP".format(str(_steps)))
        else:
            JSmod.append("{:}YEXP".format(2*" "))
        # 28-33 REFERENCE DIAMETER
        # 34-40 Y FACTOR
        JSmod.append("{:>6.6s}{:>7s}".format(str(_diameter), '1.00'))
        # 41-44 SOIL TABLE ID
        _soil_id = 'S' + str(_soil.number).zfill(3)
        JSmod.append("{:>4s}".format(_soil_id))
        # 45-60 SOIL DESCRIPTION OR OTHER REMARKS
        JSmod.append("{:>16.16s}\n".format(_soil.name))
        #
        for key in sorted(_soil.layers, key = lambda name: _soil.layers[name].depth):
            _layer = _soil.layers[key]           
            JSmod.extend(print_py(_soil.name, _soil.number, _diameter, 
                                  _layer.curve, 
                                  _layer_top, _layer.depth,
                                  _length_factor, _force_factor))
            _layer_top  = _layer.depth
        #JSmod.append("*\n")
    #
    # T-Z
    for _key1, _soil in sorted(foundation.items()):
        _diameter = _soil.diameter * _length_factor
        _layer_top = 0
        JSmod.append("*\n")
        JSmod.append("* Soil T-Z Axial Resistance, Name: {:}, Number: {:}\n"
                         .format(_soil.name, _soil.number))
        JSmod.append("*\n")        
        # 18-20 NUMBER OF SOIL STRATA
        _strata_number = str(len(_soil.layers))
        JSmod.append("SOIL TZAXIAL HEAD{:>3s} ".format(_strata_number))        
        # 22-23 MORE THAN 30 DATA POINTS FOR T-Z CURVE
        _steps = max([len(_layer.curve['TZ'].force) for _layer in _soil.layers.values()])
        if _steps > 30:
            JSmod.append("{:>2s}{:}".format(str(_steps), 4*" "))
        else:
            JSmod.append("{:}{:}".format(2*" ", 4*" "))
        # 34-40 Y FACTOR
        JSmod.append("{:>6.6s}{:>7.7s}".format('1.00','1.00'))
        # 41-44 SOIL TABLE ID
        _soil_id = 'S' + str(_soil.number).zfill(3)
        JSmod.append("{:>4s}".format(_soil_id))
        # 45-60 SOIL DESCRIPTION OR OTHER REMARKS
        JSmod.append("{:>16.16s}\n".format(_soil.name))
        #
        _spr_type = 'T-Z'
        for key in sorted(_soil.layers, key = lambda name: _soil.layers[name].depth):
            _layer = _soil.layers[key]           
            JSmod.extend(print_tz(_soil.name, _soil.number, _diameter, 
                                  _layer.curve['TZ'], _spr_type,
                                  _layer_top, _layer.depth,
                                  _length_factor, _force_factor))
            _layer_top  = _layer.depth
        JSmod.append("*\n")
    #
    # Q-Z
    for _key1, _soil in sorted(foundation.items()):
        _diameter = _soil.diameter * _length_factor
        #
        _soil_name = [{key : _soil.layers[key].curve['QZ'].name} 
                          for key in sorted(_soil.layers, key = lambda name: _soil.layers[name].depth)]        
        #
        _layer_top = 0
        JSmod.append("*\n")
        JSmod.append("* Soil T-Z End Bearing Resistance, Name: {:}, Number: {:}\n"
                         .format(_soil.name, _soil.number))
        JSmod.append("*\n")
        # SOIL T-Z END BEARING HEADER LINE
        #
        # 18-20 NUMBER OF SOIL STRATA
        _strata_number = str(len(_soil.layers))
        JSmod.append("SOIL BEARING HEAD{:>3s} ".format(_strata_number))
        #
        # 22-23 MORE THAN 30 DATA POINTS FOR Q-Z CURVE
        _steps = max([len(_layer.curve['QZ'].force) for _layer in _soil.layers.values()])
        if _steps > 30:
            JSmod.append("{:>2s}{:>10s}".format(str(_steps), " "))
        else:
            JSmod.append("{:>2s}{:>10s}".format(" ", " "))
        # 34-40 Z FACTOR
        JSmod.append("{:>7s}".format('1.00'))
        # 41-44 SOIL TABLE ID
        _soil_id = 'S' + str(_soil.number).zfill(3)
        JSmod.append("{:>4s}".format(_soil_id))
        # 45-60 SOIL DESCRIPTION OR OTHER REMARKS
        #JSmod.append("{:>16.16s}\n".format(_soil.name))
        JSmod.append("\n")
        #
        _spr_type = 'Q-Z'
        for key in sorted(_soil.layers, key = lambda name: _soil.layers[name].depth):
            _layer = _soil.layers[key]           
            JSmod.extend(print_tz(_soil.name, _soil.number, _diameter, 
                                  _layer.curve['QZ'], _spr_type,
                                  _layer_top, _layer.depth,
                                  _length_factor, _force_factor))
            _layer_top  = _layer.depth
        JSmod.append("*\n")
    #
    JSmod.append("*\n")
    JSmod.append("END\n")
    JSmod.append("*\n")
    return JSmod
#
#
def check_spring(_spr, spr_type, _diameter, 
                 _length_factor, _force_factor):
    """
    """
    #
    _symm = ''
    # get displacement
    _disp = [round((item * _length_factor), 3) 
             for item in _spr.displacement]    
    #
    if 'q-z' in spr_type.lower():
        _force = [(item/(_diameter**2 * math.pi/4.0)) / (_force_factor * _length_factor**2)
                  for item in _spr.force]
    else:
        if 't-z' in spr_type.lower():
            _force = [(item/(_diameter * math.pi)) / (_force_factor * _length_factor**2)
                      for item in _spr.force]
        else:
            _force = [round(item * _force_factor / _length_factor, 4) 
                      for item in _spr.force]
            #
            if _force[0] == 0.0 and _disp[0] == 0.0 :
                _symm = 'SM'
            else:
                f_neg = []
                for x, item in enumerate(_force):
                    if item == 0 :
                        f_pos = list(reversed(_force[x+1:]))
                        break
                    f_neg.append(abs(item))
                if f_pos == f_neg:
                    _symm = 'SM'
                    _spr.force = _force[x:]
                    _spr.displacement = _disp[x:]
                else:
                    _symm = '  '
    #
    return _spr, _symm
#
# Py
#
def print_py(_soil_name, _soil_number, _diameter,
             _layer, 
             _layer_top, _layer_bottom,
             _length_factor, _force_factor):
    """
    """
    #
    JSmod = []
    _spr_type = 'P-Y'
    _spring, _symm = check_spring(_layer['PY'], _spr_type, 
                                  _diameter, _length_factor,
                                  _force_factor)
    #
    #
    # SOIL P-Y STRATUM LINE
    #
    # 18-19 SYMMETRY INDICATOR
    JSmod.append("SOIL P-Y     SLOC{:2s}  ".format(_symm))
    # 22-23 NUMBER OF POINTS PER CURVE
    _steps = len(_spring.force)
    if _steps > 30:
        JSmod.append("{:} ".format(2*" "))
    else:
        JSmod.append("{:>2s} ".format(str(_steps)))   
    # STRATUM LOCATION
    # 25-30 TOP
    # 31-36 BOTTOM
    JSmod.append("{:>6.6s}{:>6.6s}".format(str(_layer_top), str(_layer_bottom)))
    # 37-40 P FACTOR
    # 41-44 Y SHIFT
    JSmod.append("{:>4s}{:>4s}".format('1.0', '1.0'))
    # 45-60 SOIL DESCRIPTION OR OTHER REMARKS
    JSmod.append("{:>16.16s}".format(_spring.name))
    # 70-76 HIGH PRECISION P FACTOR
    JSmod.append("{:}{:>6s}\n".format(10*" ",'1.000'))
    #
    # SOIL P-Y DATA LINE
    #nit_stress_factor = _force_factor / _length_factor
    JSmod.extend(get_spring(_spring, _spr_type))
    #
    return JSmod    
#
# TZ
#
def print_tz(_soil_name, _soil_number, _diameter,
             _layer, _spr_type,
             _layer_top, _layer_bottom,
             _length_factor, _force_factor):
    """
    """
    JSmod = []
    #
    _type_name = _spr_type
    if 'Q-Z' in _spr_type:
        _type_name = 'BEAR'
    
    #_spr_type = 'T-Z'
    _spring, _symm = check_spring(_layer, _spr_type, 
                                  _diameter, _length_factor,
                                  _force_factor)
    #
    #
    # SOIL T-Z END BEARING STRATUM LINE
    #
    # 18-19 SYMMETRY INDICATOR
    JSmod.append("SOIL {:<7.7s} SLOC{:}"
                 .format(_type_name, 4*" "))
    # 22-23 NUMBER OF POINTS PER CURVE
    _steps = len(_spring.force)
    if _steps > 30:
        JSmod.append("{:} ".format(2*" "))
    else:
        JSmod.append("{:>2s} ".format(str(_steps)))
    #
    # STRATUM LOCATION
    # 25-30 TOP
    # 31-36 BOTTOM
    JSmod.append("{:>6.6s}{:>6.6s}  ".format(str(_layer_top), str(_layer_bottom)))
    # 39-44 T FACTOR
    JSmod.append("{:>6s}".format('1.0'))
    # 45-60 SOIL DESCRIPTION OR OTHER REMARKS
    JSmod.append("{:>16.16s}\n".format(_spring.name))
    #
    JSmod.extend(get_spring(_spring, 'T-Z'))    
    #
    return JSmod    
#
#
def get_spring(_spring, _spr_type, _step=5):
    """
    """
    _force = _spring.force
    _disp = _spring.displacement    
    _steps_total = len(_force)
    #
    JSmod = []
    JSmod.append("SOIL         {:} ".format(_spr_type))
    _loop = 1
    for x in range(_steps_total):
        if x == _step:
            JSmod.append("\nSOIL         {:} ".format(_spr_type))
            _loop += 1
            _step = _step * _loop
        JSmod.append("{:>6.6s}{:>6.6s}".format(str(_force[x]), str(_disp[x])))
    JSmod.append('\n')
    #
    return JSmod
#
#
# 
# Copyright (c) 2009-2017 fem2ufo
#
#
# Python stdlib imports
import os

# package imports
import locmatrix.xl.read_xl as xlread
import locmatrix.sacs.foundations as foundations
import locmatrix.sacs.seastate as seastate
import locmatrix.sacs.loading as loading
import locmatrix.process.printing as printing


#
#
#
#
def print_foundation(soil, factors, soil_name):
    """
    """
    js_soil = foundations.print_analysis_parameters()
    js_soil.extend(foundations.print_pytzqz(soil, factors))
    file_out = 'psiinp.' + soil_name + '.inp'
    add_out = open(file_out, 'w+')
    add_out.write("".join(js_soil))
    add_out.write("".join(printing.EOF(comment_prefix="*")))
    add_out.close()
    #
#
def print_metocean(analysis, mudline,
                   case_name, factors):
    """
    """
    #
    _elevations = {}
    air_density = analysis.condition.air_density
    #i = 0
    for wdepths in analysis.condition.water_depth:
        #i+= 1
        _name = case_name # + '_' + str(i)
        # print wind areas
        _elevations[_name] = seastate.print_wind_areas(analysis.metocean_combination, 
                                                       factors)
        # print wave current
        _elevations[_name].extend(seastate.print_wave_current(analysis.metocean_combination, 
                                                              mudline, wdepths, factors))
    #
    return _elevations[_name]
    #
    #for _name, _elev in _elevations.items():
        #file_add = _name + '_sea.inp'
        #wave_out = open(file_add, 'w+')
        #wave_out.write("".join(_elev))
        #wave_out.write("".join(printing.EOF(comment_prefix="*")))
        #wave_out.close()
    #
#
#
def print_sacs(factored_sheet, actual_sheet, 
               metocean_sheet, soil_sheet,
               analysis_case, design_condition, 
               mudline, water_depth):
    """
    """
    surface = mudline + water_depth
    factors = [1, 1, 1, 1, 1, 1]
    #
    # Print foundation
    #
    if soil_sheet[0]:
        foundation, springs = xlread.get_foundation(soil_sheet[0])
        # 
        if soil_sheet[1]:
            print_foundation(foundation, factors, soil_sheet[0])
    else:
        foundation = None
        analysis_case[2] = None
    #
    # Print Metocean
    #
    ufo_meto = []
    if metocean_sheet[0] :
        metocean =  xlread.get_metocean(metocean_sheet[0])
        name = analysis_case[1]
        analysis = metocean[name]
        metocean_combination = analysis.metocean_combination
        #water_depths = analysis.condition.water_depth
        
        if metocean_sheet[1]:
            LDOPT = seastate.print_header_metocean(mudline, water_depth)
            ufo_meto = print_metocean(analysis, mudline, 
                                      analysis_case[1], factors)
    else:
        metocean_combination = None
        analysis_case[1] = None
        #water_depths = [water_depth]
    #
    # Print Load Combinations
    add_out = []
    # First level load combination
    if actual_sheet[1]:
        level_1, functional = xlread.get_basic_combinations(actual_sheet[0], 
                                                            metocean_combination,
                                                            design_condition[0])
        #
        ufo_file = []    
        _name = analysis_case[0]
        LCSEL = loading.print_load_selection(level_1)
        LCOMB = loading.print_combination_start()
        ufo_file.extend(loading.print_load_combination(level_1, 
                                                       functional, _name))
        #
        if ufo_meto:
            file_out = 'seainp.' + str(name) + '.inp'
            add_out = open(file_out, 'w+')
            add_out.write("".join(LDOPT))
            add_out.write("".join(LCSEL))
            add_out.write("".join(ufo_meto))
        else:
            file_out = str(actual_sheet[0]) + '.inp'
            add_out = open(file_out, 'w+')
        #
        add_out.write("".join(LCOMB))
        add_out.write("".join(ufo_file))
        #add_out.close()
    #
    # Second level
    if factored_sheet[1]:
        level_2, level_3 = xlread.get_factored_combinations(factored_sheet[0], 
                                                            design_condition[0])
        #
        js_file = []
        _name = factored_sheet[0]
        js_file.extend(loading.print_load_combination(level_2, level_1, 
                                                      _name, level=2))
        LCOMB = loading.print_combination_start()
        #
        if not add_out:
            file_out = str(factored_sheet[0]) + '.inp'
            add_out = open(file_out, 'w+')
            add_out.write("".join(LCOMB))
        #
        add_out.write("".join(js_file))
    #
    # close load combinations
    if add_out:
        add_out.write("".join(printing.EOF(comment_prefix="*")))
        add_out.close()
    #
    
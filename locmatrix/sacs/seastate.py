# 
# Copyright (c) 2009-2017 fem2ufo
# 


# Python stdlib imports
#import math
#from operator import itemgetter

# package imports
import locmatrix.process.printing as printing
import locmatrix.process.operations as operations

#
#
#
#
# hydro
#
def get_wave_type(lineIn):
    """
    Define wave theory
    """
    _key = {"AIRY": r"airy(\s*(\_)?linear|extrapolate)?",
            #"1.1": r"airy(\s*\_)?extretched",
            "STOK": r"stokes(\s*\_)?5",
            "3": r"user(\s*\_)?defined",
            "STRE": r"stream(\s*\_(function)?)?(8)?",
            "buoyancy": r"buoyancy",
            "calmsea": r"calm(\s*\_)?sea"}
    keyWord = operations.match_line(lineIn, _key)
    return keyWord
#
def get_wind_formula(lineIn):
    """
    """
    #
    _key = {"AS": r"\b(a(ustralian|s))\b",
            "CONS": r"\b(cons(tant)?)\b",
            "ABS": r"\b(abs)\b",
            "AP": r"\b((extreme)?ap(i)?|general)\b",
            "21AP": r"\b((extreme\_)?(\s*ap(i)?\s*)?21(\s*ap(i)?\s*)?|normal)\b"}
    keyWord, lineOut, _match = operations.select(lineIn, _key)
    return keyWord
#
#
def get_design_load(lineIn):
    """
    """
    _key = {"MS": r"\b(max(imum)?\s*b(ase)?\s*s(hear)?)\b",
            "MM": r"\b(max(imum)?\s*o(verturning|t)?\s*m(oment)?)\b",
            "NS": r"\b(min(imum)?\s*b(ase)?\s*s(hear)?)\b",
            "NM": r"\b(min(imum)?\s*o(verturning|t)?\s*m(oment)?)\b",
            "AL": r"\b((min(imum)?\s*)?both\s*(loads)?|no\s*design\s*loads)\b"}
    keyWord, lineOut, _match = operations.select(lineIn, _key)
    #
    return keyWord
#
#
def print_header_metocean(mudline, wdepths):
    """
    Print metocean header
    """
    header = 'METOCEAN DATA'
    UFOmod = printing.head_line(header, comment_prefix="*")
    
    _unit='MN'
    _water_density = '1.025'
    _struc_density = '8.2415'
    #
    UFOmod.append("LDOPT       NF+Z")
    # Physical parameters
    UFOmod.append("{:>8.8s}{:>8.8s}{:>8.8s}{:>8.8s}".format(_water_density, _struc_density,
                                                            str(mudline), str(wdepths)))
    UFOmod.append("GLOB{:}                NPNP    K\n".format(_unit))
    UFOmod.append("FILE B\n")
    UFOmod.append("*\n")
    UFOmod.append("*\n")
    return UFOmod
#
#
#
def print_wave_parameters(_wave, _wave_type, _design_load):
    """
    """
    UFOmod = []
    # Wave position parameters
    _step_size = '5.0'
    _steps = '72'
    UFOmod.append("D{:>7.7s}{:>6.6s}{:>2.2s}{:>2.2s}{:>2.2s}"
                          .format(str(_wave.phase), _step_size,
                                  " ", _steps, _design_load))
    # Member segmentation & print opt
    UFOmod.append("{:>2s}{:>2s} {:}".format('10', '1', '3'))
    # Order of stream fuction
    if 'stre' in _wave_type.lower():
        UFOmod.append("{:>2.2s}\n"
                              .format(str(int(_wave.order))))
    else:
        UFOmod.append("\n")
    
    return UFOmod
#
def print_wave_current(metocean, mudline, wdepths, factors):
    """
    Print wave and current cards is usfos format
    """
    #
    _unit='MN'
    UFOmod = []
    _section = 'Wave, Current & Wind Section'
    UFOmod.extend(printing.section_head(_section, comment_prefix='*'))
    # m
    _lenFactor = factors[0]
    # fix this m/s
    _velFactor = factors[0]

    UFOmod.append("*\n")
    UFOmod.append("LOAD\n")
    UFOmod.append("*\n")
    
    for key in sorted(metocean, key = lambda name: metocean[name].number):
        _metocean = metocean[key]
        UFOmod.append("LOADCN{:>4.4s}\n".format(str(_metocean.number)))
        UFOmod.append("LOADLB{:>4.4s} {:<59.59s}\n".format(str(_metocean.number),
                                                          str(_metocean.name)))
        # wave
        if _metocean.wave:
            _wave = _metocean.wave
            # calm sea
            if  'buoy' in _wave.theory.lower() or 'calm' in _wave.theory.lower() :
                if _metocean.current:
                    _wave_type = 'AIRY'
                    UFOmod.append("WAVE\n")
                    # Wave characteristic & still water depth                
                    UFOmod.append('WAVE1.00AIRY 0.001{:>6.6s}'.format(str(_wave.water_depth)))
                    # Wave characteristic & mudline elevation
                    UFOmod.append("{:>6.6s}{:8.8s}{:>6.6s}{:>6.6s}"
                              .format('10.', " ", 
                                      str(_metocean.wave_direction),
                                      " "))
                    #
                    _design_load = 'MU'
                    UFOmod.extend(print_wave_parameters(_wave, _wave_type, _design_load))
            # regular wave
            else:
                _wave_type = get_wave_type(_wave.theory)
                UFOmod.append("WAVE\n")
                # Wave characteristic & still water depth
                UFOmod.append("WAVE{:>4.4s}{:4.4s}{:>6.6s}{:>6.6s}"
                              .format(str(_metocean.wave_kinematics),
                                      _wave_type, str(_wave.height),
                                      str(_wave.water_depth)))
                # Wave characteristic & mudline elevation
                UFOmod.append("{:>6.6s}{:8.8s}{:>6.6s}{:>6.6s}"
                                      .format(str(_wave.period), " ", 
                                              str(_metocean.wave_direction),
                                              " "))
                #
                _design_load = get_design_load(_metocean.design_load)
                UFOmod.extend(print_wave_parameters(_wave, _wave_type, _design_load))    
        # current
        if _metocean.current:
            _curr = _metocean.current
            UFOmod.append("CURR\n")
            for i, _item in enumerate(reversed(_curr.profile)):
                _elev = round(_item[0] - mudline, 3)
                UFOmod.append("CURR{:>4s}{:>8.8s}{:>8.8s}{:>8.8s}"
                              .format(" ", str(_elev), str(_item[1]),
                                      str(_metocean.current_direction)))
                if i == 0:
                    # mudline, blocking factors
                    UFOmod.append("{:>8.8s}{:>8.8s}{:>8s}US "
                                  .format(" ",
                                          str(_metocean.current_blockage),
                                          " "))
                    # units, stretching, units
                    _stretching = 'NL'
                    if 'off' in _metocean.current_stretching:
                        _stretching = 'CN'                    
                    UFOmod.append("{:>2.2s} {:>2.2s} {:>2.2s}"
                                  .format(_stretching, " ", " "))
                UFOmod.append("\n")
        # wind
        if _metocean.wind:
            _wind = _metocean.wind
            UFOmod.append("WIND\n")
            UFOmod.append("WIND1DW{:}".format(" "))
            #
            _formula = get_wind_formula(_wind.formula)
            _ref_height = str(_wind.height * _lenFactor)
            if _formula == 'AP':
                _formula = _formula + str(int(1.0/_win.power)).zip(2)
            elif _formula == '21AP':
                _ref_height = str(_wind.duration) 
            #
            _velocity = str(_wind.velocity * _velFactor)
            # wind load characteristic, wind direction
            UFOmod.append("{:>8.8s}{:>8.8s}{:>8.8s}"
                          .format(_velocity, _ref_height, 
                                  str(_metocean.wind_direction)))
            # still water & wind formula
            UFOmod.append("{:>8.8s}{:>4.4s}".format(" ", _formula))
            # areas
            if _metocean.wind_areas:
                for _area in _metocean.wind_areas:
                    _name = _area.name.replace('wind_area_', '')
                    UFOmod.append("{:>2.2s}".format(_name))
                UFOmod.append("\n")
        UFOmod.append("*\n")
    #
    return UFOmod
#
def print_wind_areas(metocean, factors):
    """
    """
    #
    UFOmod = []
    _section = 'Wind Areas Section'
    UFOmod.extend(printing.section_head(_section, '*'))    
    # m
    _lenFactor = factors[0]
    _unit='MN'
    
    for key in sorted(metocean, key = lambda name: metocean[name].number):
        _metocean = metocean[key]
        if _metocean.wind_areas:
            for x, _area in enumerate(_metocean.wind_areas):
                # area ID
                UFOmod.append("AREA{:>2.2s}".format(_area.name))
                # Area specification
                _proyection = [str(_area._size[0]*_area._size[0]*_lenFactor**2) 
                               if _item else " " 
                               for _item in _area.wind_pressure]
                UFOmod.append("{:>6.6s}{:>6.6s}{:>6.6s}".format(*_proyection))
                # Centroidal Location
                _CoG = [str(_item*_lenFactor) for _item in _area.mass[1:]]
                UFOmod.append("{:>7.7s}{:>7.7s}{:>7.7s}".format(*_CoG))
                # Shape factor
                _shape_factor = [_item[0] for _item in _area.wind_pressure
                                 if _item]
                _shape_factor = str(sum(_shape_factor)/len(_shape_factor))
                UFOmod.append("{:>5.5s}".format(_shape_factor))
                # Distribution joints
                for x in range(7):
                    try:
                        _joint = _area.joints[x]
                        if not _joint:
                            _joint = " "
                    except IndexError:
                        _joint = " "
                    
                    try:
                        UFOmod.append("{:>4.4s}".format(_joint))
                    except ValueError:
                        UFOmod.append("{:>4.4s}".format(str(int(_joint))))
                #
                UFOmod.append("F\n")
    #
    UFOmod.append("*\n")
    return UFOmod
#
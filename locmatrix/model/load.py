#
# Copyright (c) 2017 LoCmatrix
# 

# Python stdlib imports
import sys
from collections import namedtuple

# package imports
#

#
# --------------------
# LOAD Clases
# --------------------
class LoadType:
    """
    FE Load Cases
    
    LoadType
        |_ name
        |_ number
        |_ functional
        |_ combination_level
        |_ time_domain
        |_ frequency_domain
        |_ temperature
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'functional',
                 'time_domain', 'frequency_domain',
                 'temperature')
    #
    def __init__(self, Number, Name='N/A'):
        #
        self.number = Number
        self.name = Name
        self.functional = {}
        #self.combination = {}
        self.time_domain = {}
        self.frequency_domain = {}
        self.temperature = {}
        #

#
#
class BasicLoad:
    """
    FE Load Class
    
    Load
        |_ name
        |_ number
        |_ node [nd1, nd2, nd3,..., ndi]
        |_ element [elm1, elm2, elm3,..., elmi]
        |_ time_series [th[0], th[1], th[2],..., th[n]]
        |_ gravity [x, y, z, mx, my, mz]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'node', 'element', 'gravity',
                 'sets', 'time_series', 'class_type')
    #
    def __init__(self, Number, Name='N/A'):
        """
        """
        #
        self.number = Number
        self.name = Name
        self.node = []
        self.element = []
        self.gravity = []
        self.time_series = []
        #
        self.class_type = 'basic load'
    #
    def get_name(self, number=False, name=False):
        """
        """
        if not number:
            number = self.number
        #
        if not name:
            name = self.name
        
        self.name = 'BLC' + str(number).zfill(3) + '_' + str(name)
#
#
class LoadCombination:
    """
    FE Load Combination Class
    
    LoadCombination
        |_ name
        |_ number
        |_ design_condition : operating/storm
        |_ functional_load [load number, factor]
        |_ wave_load [load number, factor]
        |_ wind_load [load number, factor]
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'functional_load',
                 'level', 'metocean_load', 
                 'design_condition', 'class_type')
    #
    def __init__(self, Number, Name, level=0):
        #
        self.name = Name
        self.number = Number
        self.level = level
        self.functional_load = []
        self.metocean_load = []
        #
        #self.analysis = None
        self.class_type = 'combination'
    #
    def get_name(self, number=False, name=False, level=False):
        """
        """
        if not number:
            number = self.number
        #
        if not name:
            name = self.name
        #
        if not level:
            level = self.level
        
        self.name = 'LC' + str(number).zfill(3) + '_' + str(name)
#
#
Size = namedtuple('Size', ['length', 'height', 'width'])
Mass = namedtuple('Mass', ['mass', 'x', 'y', 'z'])
windPressure = namedtuple('wind_pressure', ['drag_coefficient', 'suction_factor'])
#
class Equipment:
    """
    name
    number
    size = [length, height, width]
    mass = [mass, CoGx, CoGy, CoGz]
    wind_pressure [drag_coefficient, suction_factor]
    """
    __slots__ = ('number', 'name', 'type',
                 '_mass', '_size', 'foot_print',
                 'wind_pressure', 'joints',
                 'load_interface')
    
    def __init__(self, name, size=None):
        self.name = name
        self._size = size
        self._mass = None
        self.wind_pressure = [None, None, None]
        self.joints = []
        self.load_interface = []
    #
    @property
    def size(self):
        """
        """
        return self._size
    @size.setter
    def size(self, value):
        """
        """
        self._size = Size(*value)
    #
    @property
    def mass(self):
        """
        """
        return self._mass
    @mass.setter
    def mass(self, mass):
        """
        """
        _mass = mass[0]
        CoG = mass[1]
        if not CoG:
            self._mass = Mass(_mass, 
                              self._size.length/2.0,
                              self._size.height/2.0,
                              self._size.width/2.0)
        else:
            self._mass = Mass(_mass, CoG[0], CoG[1], CoG[2])
    #
    def wind_pressure_x(self, *value):
        """
        """
        self.wind_pressure[0] = windPressure(value[0], value[1])
    
    def wind_pressure_y(self, *value):
        """
        """
        self.wind_pressure[1] = windPressure(value[0], value[1])
    
    def wind_pressure_z(self, *value):
        """
        """
        self.wind_pressure[2] = windPressure(value[0], value[1])    
#
#
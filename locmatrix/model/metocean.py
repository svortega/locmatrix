# 
# Copyright (c) 2009-2015 fem2ufo
# 


# Python stdlib imports


# package imports

#
#
# HYDRODYNAMICS Section
class Metocean():
    """
    FE Metocean Class
    
    Metocean
        |_ name
        |_ number
        |_ data
        |_ type
        |_ wave
        |_ current
        |_ wind
        |_ cdcm
        |_ non_hydro
        |_ elevation
        |_ hydro_diametre
        |_ buoyancy
        |_ flooded
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    # TODO: need to remove cdcm, this is member property
    __slots__ = ('number', 'name', 'wave', 'current', 'data',
                 'marine_growth', 'wind', 'cdcm', 'non_hydro', 
                 'elevation', 'hydro_diameter', 'buoyancy',
                 'flooded')
    #
    def __init__(self, number, name):
        #
        self.number = number
        self.name = name
        self.elevation = {}
#
#
class Condition():
    """
    FE Metocean Condition Class
    
    Condition
        |_ name
        |_ number
        |_ surface
        |_ mudline
        |_ gravity
        |_ water_density
        |_ air_density
        |_ fluid_viscosity
        |_ air_viscosity
        |_ water_depth
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    __slots__ = ('number', 'name', 'surface', 'mudline',
                 'gravity', 'water_density', 'air_density',
                 'fluid_viscosity', 'air_viscosity', 
                 'water_depth')

    def __init__(self, number, name):
        self.number = number
        self.name = name
        # self.mudline = 0
        self.surface = 0
        #
        self.gravity = 9.81  # m/s2
        # Density (especific mass)
        self.water_density = 1025.0  # kg/m3
        self.air_density = 1.226  # kg/m3
        # Kinematic viscosity
        self.fluid_viscosity = 1.19e-6  # m2/s
        self.air_viscosity = 1.462e-5  # m2/s
        #
        self.water_depth = []
#
#
class Wave():
    """
    FE Wave Class
    
    Wave
        |_ name
        |_ number
        |_ theory
        |_ order
        |_ height
        |_ period
        |_ phase
        |_ direction
        |_ cdcm
        |_ time
        |_ step
        |_ number_step
        |_ kinematics
        |_ current
        |_ c_blockage
        |_ c_stretching
        |_ buoyancy
        |_ wind
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'theory', 'height', 'period', 'water_depth',
                 'phase', 'time', 'step', 'number_step', 'order', 'direction',
                 'items')
    #
    def __init__(self, number, name):
        #
        self.number = number
        self.name = name
        self.order = None
        self.theory = None
        #
        self.phase = 0
        #
        self.water_depth = None
        self.items = []
        #
#
#
class Current():
    """
    FE Current Class
    
    Current
        |_ name
        |_ number
        |_ profile
        |_ items
        |_ sets
        |_ absolute_elevation : True if elevations are relative to seabed, 
                                False if elevations are relative SWL Z (default)
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'profile',
                 'velocity', 'alignment', 'direction',
                 'items', 'sets', 'absolute_elevation',
                 'class_type')
    #
    def __init__(self, number, name):
        #
        self.number = number
        self.name = name
        # CurrentProfile
        self.profile = []
        # Current Velocity
        self.items = []
        self.sets = {}
        #
        self.absolute_elevation = False
        #
        self.class_type = 'current'

#
#
class Wind():
    """
    FE Wind Class
    
    Wind
        |_ name
        |_ number
        |_ type
        |_ 
        |_ items
        |_ sets
    
    **Parameters**:  
      :number:  integer internal number 
      :name:  string node external name
    """
    #
    __slots__ = ('number', 'name', 'items', 'sets',
                 'velocity', 'alignment', 'gust_factor', 'height',
                 'period_ratio', 'formula', 'power', 'method', 
                 'density', 'class_type', 'direction')
    #
    def __init__(self, number, name):
        #
        self.number = number
        self.name = name
        self.power = 0.20
        self.items = []
        self.sets = {}
        #
        self.class_type = 'wind'
        self.alignment = 'x axis'
        self.gust_factor = 1.0
        self.period_ratio = 1.0 
#
#
class MetoceanCombination():
    """
    FE MetoceanCombination Class
    
    MetoceanCombination
                      |_ name
                      |_ number
    """
    __slots__ = ('number', 'name', 'buoyancy', 'condition',
                 'wave', 'wave_direction', 'wave_kinematics', 'wave_doopler',
                 'wind', 'wind_direction', 'wind_method',
                 'current', 'current_direction', 'current_blockage',
                 'current_stretching', 'design_load',
                 'report', 'class_type', 'wind_areas')
    #
    def __init__(self, number, name):
        #
        self.number = number
        self.name = name
        #
        self.wave = None
        self.wave_direction = None
        self.wave_kinematics = 1.0
        self.wave_doopler = None
        #
        self.current = None
        self.current_direction = None
        self.current_blockage = 1.0
        self.current_stretching = False
        #
        self.wind = None
        self.wind_direction = None
        self.wind_method = None
        self.wind_areas = []
        #
        self.buoyancy = None
        #
        self.class_type = 'metocean'
        self.design_load = 'NoDesignLoads'
        #
    #
    def get_name(self, number=False, name=False):
        """
        """
        if not number:
            number = self.number
        #
        if not name:
            name = self.name
        #
        self.name = 'MET' + str(number).zfill(3) + '_' + str(name)
    #
    #
    #
#


#
# Copyright (c) 2009-2016 fem2ufo
# 

# Python stdlib imports



# package imports
#


#
#
class Analysis():
    """
    """
    __slots__ = ('number', 'name', 'case')
    #
    def __init__(self, number, name):
        #
        self.number = number
        self.name = name
        self.case = {}
#
#
class Case():
    """
    """
    __slots__ = ('number', 'name', 'condition',
                 'design_condition', 
                 'metocean_combination',
                 'load_combination')
    
    def __init__(self, number, name):
        #
        self.number = number
        self.name = name
        self.metocean_combination = {}
        self.load_combination = {}
#
#
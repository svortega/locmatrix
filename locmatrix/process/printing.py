# 
# Copyright (c) 2009-2017 fem2ufo
# 


# Python stdlib imports
#import math

# package imports


def head_line(head, comment_prefix, length=84):
    """
    """
    FEMufo = []
    _head = str(head).upper()
    _space = length - 2 - len(_head)
    FEMufo.append("{:}\n".format(comment_prefix))
    FEMufo.append("{:}\n".format(comment_prefix))
    FEMufo.append("{:}{:}\n".format(comment_prefix, length * '-'))
    FEMufo.append("{:}{:} {:}\n".format(comment_prefix, _space * " ", _head))
    FEMufo.append("{:}{:}\n".format(comment_prefix, length * '-'))
    FEMufo.append("{:}\n".format(comment_prefix))
    FEMufo.append("{:}\n".format(comment_prefix))
    return FEMufo
#
def section_head(section_name, comment_prefix,
                 column=85):
    """
    """
    section_name = section_name.strip()
    _step = column - len(section_name) - 3
    UFOmod = []
    UFOmod.append("{:}\n".format(comment_prefix))
    UFOmod.append("{:}{:} {:}\n"
                  .format(comment_prefix,
                          _step * "-", section_name))
    UFOmod.append("{:}\n".format(comment_prefix))
    return UFOmod
#
def EOF(comment_prefix, length=84):
    ""
    ""
    JSmod = []
    JSmod.append("{:}\n".format(comment_prefix))
    JSmod.append("{:}{:}\n".format(comment_prefix, length * '-'))
    JSmod.append("{:}{:} LoCmatrix is Python Powered \n"
                 .format(comment_prefix, 32 * " "))
    JSmod.append("{:}{:} http://www.python.org/ \n"
                 .format(comment_prefix,34 * " "))
    JSmod.append("{:}{:} EOF \n".format(comment_prefix, 43 * " "))
    JSmod.append("{:}{:}\n".format(comment_prefix, length * '-'))
    JSmod.append("{:}\n".format(comment_prefix))
    return JSmod
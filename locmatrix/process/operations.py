# 
# Copyright (c) 2017 LoCmatrix
# 


# Python stdlib imports
#import math
import re

# package imports

def match_line(lineIn, key):
    """
    """
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.match(lineIn)
        if keys:  keyWord = _key
    return keyWord
#
#
def select(lineIn, key, keyWord=None, count=1):
    """
    match
    """
    #
    lineOut = lineIn
    _match = False
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.match(lineIn)
        if keys:
            keyWord = _key
            lineOut = re.sub(keys.group(), " ", lineIn, count)
            _match = True
    #
    lineOut = lineOut.strip()
    return keyWord, lineOut, _match
#
#
def search_line(lineIn, key, keyWord=None, count=1):
    """
    search key word anywere in the the string
    """
    lineOut = lineIn
    _match = False
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.search(lineIn)
        if keys:
            keyWord = _key
            lineOut = re.sub(keys.group(), " ", lineIn, count)
            _match = True
    #
    lineOut = lineOut.strip()
    return keyWord, lineOut, _match


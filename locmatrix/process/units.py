#
# Copyright (c) 2017 LoCmatrix
#  

# Python stdlib imports
import re
import math
import sys

# package imports
from locmatrix.process.buckingham import Number
import locmatrix.process.operations as operations


#
#
#
#
#
#
def find_length_unit(lineIn, out=False):
    """
    lineIn : string
    out : False --> return unit length (default)
        : True  --> return unit length and rest of input string
    """
    _key = {"metre": r"\b(m(et(r)?e(r)?(s)?)?)\b",
            "decimetre": r"\b(d(eci\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "centimetre": r"\b(c(enti\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "millimetre": r"\b(m(illi\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "decametre": r"\b(d(e(ck)?)?am(et(r)?e(r)?(s)?)?)\b",
            "hectometre": r"\b(h(ecto\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "kilometre": r"\b(k(ilo\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "nanometre": r"\b(n(ano\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "micrometre": r"\b(micro[n]?|u(m(et(r)?e(r)?(s)?)?)?)\b",
            # US units
            "foot": r"\b(f(oo|ee)?t|\')(s)?\b",
            "inch": r"\b(in(ch(es)?)?|\"|\'')\b",
            "yard": r"\b(y(ar)?d(s)?)\b",
            "mile": r"\b((statute|land)?mi(le)?)(s)?\b"}

    keyWord, lineOut, _match = operations.search_line(lineIn, _key)
    
    if out:
        return keyWord, lineOut

    return keyWord
#
def find_mass_unit(lineIn):
    """
    """
    _key = {"1.0*gram": r"\b(g(ram(me)?(s)?)?)\b",
            "1.0*decigram": r"\b(d(eci)?g(ram(me)?(s)?)?)\b",
            "1.0*centigram": r"\b(c(enti)?g(ram(me)?(s)?)?)\b",
            "1.0*milligram": r"\b(m(illi)?g(ram(me)?(s)?)?)\b",
            "1.0*microgram": r"\b((m[i]?c(ro)?|u)g(ram(me)?(s)?)?)\b",
            "1.0*nanogram": r"\b(n(ano)?g(ram(me)?(s)?)?)\b",
            "1.0*picogram": r"\b(p(ico)?g(ram(me)?(s)?)?)\b",
            "1.0*kilogram": r"\b(k(ilo)?g(ram(me)?(s)?)?)\b",
            "1.0*hectogram": r"\b(h(ecto)?g(ram(me)?(s)?)?)\b",
            "1.0*decagram": r"\b(d(e[ck]?)?ag(ram(me)?(s)?)?)\b",
            "1.0*megagram": r"\b((M|mega\s*)g(ram(me)?(s)?)?|(metric\s*)?tonne(s)?)\b",
            # US units
            "1.0*pound": r"\b((pound(s)?|lb)(\s*m(ass)?)?)\b",
            "0.031081*pound": r"\b(slug(s)?)\b",
            "2000*pound": r"\b((short\s*)?ton(s)?)\b",
            "2240*pound": r"\b((long|weight|imperial)\s*ton(s)?)\b"}

    keyWord, lineOut, _match = operations.search_line(lineIn, _key)

    return keyWord
#
def find_force_unit(lineIn, out=False):
    """
    """
    _key = {"newton": r"\bn(ewton(s)?)?\b",
            "kilonewton": r"\bk(ilo)?\s*n(ewton(s)?)?\b",
            "meganewton": r"\bm(ega)?\s*n(ewton(s)?)?\b",
            # US units
            "lbf": r"\b(pound(s)?|lb)(\s*f(orce)?)?\b",
            "kilolbf": r"\b(k(i(lo)?)?(\s*p(ound)?|lb)(s)?)(\s*f(orce)?)?\b"}

    keyWord, lineOut, _match = operations.search_line(lineIn, _key)
    
    if out:
        return keyWord, lineOut

    return keyWord
#
def find_pressure_unit(lineIn, out=False):
    """
    """
    # stress
    _key = {"pascal": r"\b(Pa(scal)?(s)?)\b",
            "hectopascal": r"\b(h(ecto\s*)?Pa|pascal(s)?)\b",
            "kilopascal": r"\b(k(ilo\s*)Pa|pascal(s)?)\b",
            "megapascal": r"(M|mega\s*)Pa|pascal(s)?|((N|newton(s)?)\/(m(illi)?m(et(r)?e(r)?(s)?)?(\**|\^)?2))",
            "gigapascal": r"(G|giga\s*)Pa|pascal(s)?",
            # pressure
            "bar": r"bar",
            "megabar": r"(M|mega\s*)bar",
            "kilobar": r"k(ilo\s*)bar",
            "decibar": r"d(eci\s*)?bar",
            "centibar": r"c(enti\s*)?bar",
            "millibar": r"m(illi\s*)?b(ar)?",
            # US units
            "psi": r"\b((p(ound(s)?)?|lb)(\s*f(orce)?)?(\s*per|\/)?(\s*s(quare)?)\s*i[n]?(che(s)?)?((\**|\^)?2)?)",
            "kilopsi": r"\b(k(ilo\s*)((p(ound(s)?)?|lb)(\s*f(orce)?)?)?(\s*per|\/)?(\s*s(quare)?)\s*i[n]?(che(s)?)?((\**|\^)?2)?)\b"}

    keyWord, lineOut, _match = operations.search_line(lineIn, _key)
    
    if out:
        return keyWord, lineOut

    return keyWord


#
def find_velocity_unit(lineIn, out=False):
    """
    """
    _key = {"metre": r"\b(m(et(r)?e(r)?(s)?)?)\b",
            "decimetre": r"\b(d(eci\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "centimetre": r"\b(c(enti\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "millimetre": r"\b(m(illi\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "decametre": r"\b(d(e(ck)?)?am(et(r)?e(r)?(s)?)?)\b",
            "hectometre": r"\b(h(ecto\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "kilometre": r"\b(k(ilo\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "nanometre": r"\b(n(ano\s*)?m(et(r)?e(r)?(s)?)?)\b",
            "micrometre": r"\b(micro[n]?|u(m(et(r)?e(r)?(s)?)?)?)\b",
            # US units
            "foot": r"\b(f(oo|ee)?t|\')(s)?\b",
            "inch": r"\b(in(ch(es)?)?|\"|\'')\b",
            "yard": r"\b(y(ar)?d(s)?)\b",
            "mile": r"\b((statute|land)?mi(le)?)(s)?\b",
            "0.8690 * mile" : r"knot(s)?"}

    keyWord, lineOut, _match = operations.search_line(lineIn, _key)
    
    if out:
        return keyWord, lineOut

    return keyWord
#
# Acceleration
def find_acceleration_unit(lineIn):
    """
    """
    _key = {"9.80665 metre/second^2": r"\b(g(ravity)?)\b"}

    keyWord, lineOut, _match = operations.search_line(lineIn, _key)

    return keyWord


#
# ---
#
#
#
def find_unit_case(word_in):
    """
    """
    _key = {"length": r"\b(l(a|e)ng(th|[d]?e)?|long(itud|ueur)?|largo)\b",
            "force": r"\b(force|fuerza|kraft)\b",
            "mass": r"\b(mass(e[n]?)?|masa)\b",
            "temperature": r"\b(temp(eratur(e|a)?)?)\b",
            "acceleration": r"\b(acc(eleration)?)\b", }

    #
    _match = operations.match_line(word_in, _key)

    if not _match:
        print('  **  error unit {:} not recognized'.format(word_in))
        print('      process terminated')
        sys.exit()

    return _match
#
#
def units_module(_unit, item, _unit_list=False):
    """
    Units [length, mass, time, temperature, force, pressure/stress]
    """
    if not _unit_list:
        _unit_list = ["", "", "second", "", "", ""]

    if _unit == 'length':
        item = find_length_unit(item)
        _unit_list[0] = item

    elif _unit == 'mass':
        item = find_mass_unit(item)
        _unit_list[1] = item

    elif _unit == 'time':
        item = find_mass_unit(item)
        _unit_list[2] = item

    elif _unit == 'temperature':
        item = find_mass_unit(item)
        _unit_list[3] = item

    elif _unit == 'force':
        item = find_force_unit(item)
        _unit_list[4] = item

    elif _unit == 'pressure':
        item = find_force_unit(item)
        _unit_list[5] = item

    elif _unit == 'acceleration':
        item = find_acceleration_unit(item)
        _unit_list[5] = item

    else:
        print('  *** error unit {:} not recognized'.format(_unit))
        print('      process terminated')
        sys.exit()

    return _unit_list


#
# ---
#
#
def find_unit_system(_InputUnit):
    #
    _english = ['foot', ' inch', 'mile', 'nautical-mile', 'fathom', 'yard', 'rod', 'furlong',
                'pound', 'slug', 'pound-force', 'ounce-force', 'pound', 'ounce', 'ton', 'long-ton',
                'hundredweight', 'dram' 'grain', 'pennyweight', 'scruple', 'acre', 'square-mile',
                'cubic-inch', 'cubic-foot', 'cubic-yard', 'cubic-mile', 'acre-foot', 'gallon',
                'quart', 'peck', 'bushel', 'fifth', 'pint', 'cup', 'fluid-ounce', 'gill', 'fluidram',
                'minim', 'tablespoon', 'teaspoon', 'foot-pound', 'horsepower-hour', 'grain',
                'horsepower', 'british-thermal-unit', 'btu', 'pounds-per-square-inch', 'psi',
                'miles-per-hour', 'miles-per-second', 'feet-per-second', 'knot',
                'square-foot', 'square-yard', 'square-inch']
    #
    if _InputUnit in _english:
        _OutUnit = 'US'
    else:
        _OutUnit = 'SI'
    #
    return _OutUnit
    #


#
#
#
def get_factors_and_gravity(_input, _output):
    """
    factors : Units [length, mass, time, temperature, force, pressure/stress]
    """
    # Input units
    factors = [0, 0, 0, 0, 0, 0]
    #
    # length
    _dim0 = _input[0]
    _length = Number(1, dims=_dim0)
    # time
    _dim2 = _input[2]
    # force
    _dim4 = _input[4]
    _force = Number(1, dims=_dim4)
    # pressure
    try:
        _dim5 = _input[5]
        if not _dim5:
            _dim5 = str(_dim4) + '/' + str(_dim0) + '^2'
    except IndexError:
        _dim5 = str(_dim4) + '/' + str(_dim0) + '^2'

    _pressure = Number(1, dims=_dim5)
    # grav
    gravity = Number(9.80665, dims='metre')

    #  set units
    _dim0out = _output[0]
    _grav = gravity.convert(_dim0out).value
    factors[0] = _length.convert(_dim0out).value

    _dim4out = _output[4]
    factors[4] = _force.convert(_dim4out).value

    try:
        _dim5out = _output[5]
        if not _dim5out:
            _dim5out = str(_dim4out) + '/' + str(_dim0out) + '^2'
    except IndexError:
        _dim5out = str(_dim4out) + '/' + str(_dim0out) + '^2'
    factors[5] = _pressure.convert(_dim5out).value

    # TODO: check this works
    try:
        _dim1 = _input[1].split('*')
        _mass_temp = float(_dim1[0]) * Number(1, dims=_dim1[1])
        _mass_out = _output[1].split('*')
        factors[1] = float(_mass_out[0]) * _mass_temp.convert(_mass_out[1]).value

    except:
        _grav_in = _dim4 + '*second^2/' + _dim0
        _mass_in = Number(1, dims=_grav_in)
        _grav_out = _dim4out + '*second^2/' + _dim0out
        factors[1] = _mass_in.convert(_grav_out).value
    #
    # print(_mass_temp.units(), _mass_temp.value)
    #
    return factors, _grav


#
def get_tolerance(length_unit, tolerance=0.1):
    """
    set tolerance = 10 cm
    """
    # 
    _dim0 = length_unit
    _toldim = Number(tolerance, dims='metre')
    _tol = _toldim.convert(_dim0).value

    return _tol


#
#
def convert_units_SI(_length, _force):
    """
    Module to convert input units in the default metric units:
    [metre', 'gram', 'second', 'kelvin', 'newton', 'pascal]
    """
    _conv = Number(1.0, dims=_length)
    _lengthFactor = _conv.value

    _conv = Number(1.0, dims=_force)
    # N/m^2  without conversion result is: g/(m*s^2)
    _forceFactor = _conv.value/1000.0

    return _lengthFactor, _forceFactor
    #
#
def convert_units_area_SI(_input):
    """
    """
    _input = _input.replace('2', '')
    _length, _item = find_length_unit(_input, out=True)
    _conv = Number(1.0, dims=_length)
    return _conv.value
#
def convert_units_length_SI(_input):
    """
    """
    _length, _item = find_length_unit(_input, out=True)
    _conv = Number(1.0, dims=_length)
    return _conv.value
#
def convert_units_force_SI(_input):
    """
    """
    _length, _item = find_force_unit(_input, out=True)
    _conv = Number(1.0, dims=_length)
    _item = _item.strip()
    return _item, _conv.value
#
def convert_units_pressure_SI(_input):
    """
    """
    _length, _item = find_pressure_unit(_input, out=True)
    _conv = Number(1.0, dims=_length)
    _item = _item.strip()
    return _item, _conv.value
#
#
def convert_units_velocity_SI(_input):
    """
    """
    _factor = 1.0
    _length, _item = find_velocity_unit(_input, out=True)
    if '*' in _length:
        _length = _length.split('*')
        _factor = float(_length[0])
        _length = _length[1]
    _conv = Number(1.0, dims=_length)
    return _conv.value * _factor
#
#
# TODO : to be completed
# Acceleration
def find_acceleration_unit2(InputUnit):
    #
    _InputUnit = str(InputUnit).lower()
    _InputUnit = _InputUnit.replace(' ', '')
    _InputUnit = _InputUnit.replace('-', '')
    _InputUnit = _InputUnit.replace('_', '')
    _InputUnit = _InputUnit.strip()
    #
    _earth_gravity = ['earth-gravity']
    _gravity = ['gravity', 'g']
    _feet_per_second_squared = ['foot-per-second-squared',
                                'ft/s/s', 'ft/sec/sec',
                                'foot/sec/sec', 'ft/s2',
                                'ft/sec2', 'foot/second/second']
    _metres_per_second_squared = ['metre-per-second-squared',
                                  'm/s/s', 'm/sec/sec',
                                  'm/second/second', 'm/s2',
                                  'm/sec2', 'metre/sec/sec',
                                  'metre/second/second']
    _centimetres_per_second_squared = ['centimetre-per-second-squared',
                                       'cm/s/s', 'cm/sec/sec', 'cm/s2',
                                       'cm/sec2']
    #
    #

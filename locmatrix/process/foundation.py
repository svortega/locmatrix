# Copyright (c) 2017 LoCmatrix

#
# Python stdlib imports
#import os

# package imports
#import xlwings as xw
import locmatrix.xl.read_xl as xlread

#
def get_soil_data(workbook, soil_sheet, check_sheet):
    """
    """
    # get excel workbook
    sheet = workbook.sheets[check_sheet]
    foundations, springs = xlread.get_foundation(soil_sheet[0])
    
    #print_curves(sheet, springs, soil_sheet[1])
    for key, _foundation in foundations.items():
        i = 0
        for _name, _layer in _foundation.layers.items():
            i += 2
            sheet.range((10+i-1, 10)).value = 'depth_' + str(_layer.depth)
            _spring = _layer.curve[soil_sheet[1]]
            _force = _spring.force
            _displacement = _spring.displacement
            _index = _displacement.index(0)
            if _force[_index] == 0:
                sheet.range((10+i-1, 11)).value = _displacement[_index:]
                sheet.range((10+i, 11)).value = _force[_index:]
    #
    #print('-->')
#
#
def print_curves(sheet, springs, curve_name):
    """
    """
    i = 0
    for key, _spring in sorted(springs.items()):
        if curve_name in key.upper():
            _force = _spring.spring['force']
            _displacement = _spring.spring['displacement']
            _index = _displacement.index(0)
            if _force[_index] == 0:
                i += 2
                sheet.range((4, 10+i-1)).value = _spring.name
                sheet.range((5, 10+i-1)).options(transpose=True).value = _displacement[_index:]
                sheet.range((5, 10+i)).options(transpose=True).value = _force[_index:]
    #
    #
#
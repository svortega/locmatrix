# Copyright (c) 2017 LoCmatrix
#
# Python stdlib imports
import os

# package imports
import xlwings as xw


#
#
def get_combinations(sheet_name='Master'):
    """
    """
    #
    from locmatrix.xl.read_xl import get_master
    #
    # get excel workbook
    wb = xw.Book.caller()
    # get sheet path
    path_name = xw.sheets[sheet_name].range('G1').value
    path = os.path.normcase(path_name)
    # change working directory
    os.chdir(path)
    # get master sheet data
    _case = get_master(sheet_name)
    #
    factored_sheet = _case['factored']
    actual_sheet = _case['actual']
    metocean_sheet = _case['metocean']
    foundation_sheet = _case['foundation']
    mudline = _case['mudline']
    water_depth = _case['water_depth']
    analysis = _case['analysis']
    design_condition = _case['design_condition']
    # Process data
    if _case['format'].lower() == "genie":
        from locmatrix.genie.genie_out import print_js as print_matrix
    
    elif _case['format'].lower() == "usfos":
        from locmatrix.usfos.usfos_out import print_ufo as print_matrix
    
    elif _case['format'].lower() == "sacs":
        from locmatrix.sacs.sacs_out import print_sacs as print_matrix
    # print data
    print_matrix(factored_sheet,
                 actual_sheet,
                 metocean_sheet,
                 foundation_sheet,
                 analysis,
                 design_condition,
                 mudline, water_depth)
    #print('--->')
    #
#
def soil_check(sheet_name='Health_Checks'):
    """
    """
    #
    from locmatrix.xl.read_xl import get_health_checks
    from locmatrix.process.foundation import get_soil_data
    #
    # get excel workbook
    wb = xw.Book.caller()
    # get sheet path
    path_name = xw.sheets[sheet_name].range('G1').value
    path = os.path.normcase(path_name)
    # change working directory
    os.chdir(path)
    
    # get master sheet data
    _case = get_health_checks(sheet_name)    
    foundation_sheet = _case['foundation']
    
    get_soil_data(wb, foundation_sheet, sheet_name)
    
    #print('--->')
#

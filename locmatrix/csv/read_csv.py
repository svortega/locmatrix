# 
# Python stdlib imports
#import re
#import os
#import math
#import datetime
import csv

# package imports
#from xlwings import Workbook, Range, Sheet
import xlwings as xw


#
file_name = 'Load_Combination_Factored.csv'

def get_csv(file_name):
    """
    """
    #
    with open(file_name, 'r') as f:
        reader = csv.reader(f)
        lcomb = list(reader)
    #
    #
    _total = len(lcomb)
    _basic = {}
    for x in range(1, _total):
        _name = lcomb[x][0]
        _basic[_name] = x
    #
    #
    _total = len(lcomb[0])
    _comb = {}
    for x in range(4, _total):
        _name = lcomb[0][x]
        _comb[_name] = x
    #
    return lcomb, _basic, _comb
#
#
#
def fill_sheet(file_name, spreadsheet, sheet_name):
    """
    """
    #
    lcomb, _basic, _comb = get_csv(file_name)
    #
    wb = xw.Workbook(spreadsheet)
    #
    j = 0
    list_lc = []
    for name2, _index2 in sorted(_comb.items()):
        xw.Range(sheet_name, (2, 5+j)).value  = name2
        # xw.Range(sheet_name, (3, 5+j)).value  = lcomb[0][_index2]
        list_lc.append(_index2)
        j += 1
        #
    #
    i = 0
    for name, _index in sorted(_basic.items()):
        # name
        xw.Range(sheet_name, (4+i, 1)).value  = name
        # number
        #xw.Range(sheet_name, (4+i, 2)).value  = lcomb[_index][1]
        #
        j = 0
        for _index2 in list_lc:
            xw.Range(sheet_name, (4+i, 5+j)).value  = lcomb[_index][_index2]
            j += 1
        #
        i += 1
#
#
file_name = 'Load_Combination_Factored.csv'
#
spreadsheet = r'D:\MySyncFolder\load_combination\bplocmatrix_00.xlsx'
#sheet_name = 'Factored_Load_Combination'
sheet_name = 'Current_Load_Combination'
#
fill_sheet(file_name, spreadsheet, sheet_name)
#
print('ok')
#
#

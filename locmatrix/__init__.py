#
#
from locmatrix.interface.locmatrix import get_combinations, soil_check
#
#from locmatrix.xl.read_xl import (get_basic_combinations,
#                                  get_factored_combinations,
#                                  get_metocean, get_master)
#
#from locmatrix.genie.loading import (print_top_load_combinations,
#                                     print_load_combinations)

#from locmatrix.genie.genie_out import print_js

#__all__ = "interface"


# constants

__major__ = 0.  # for major interface/format changes
__minor__ = 2  # for minor interface/format changes
__release__ = 1  # for tweaks, bug-fixes, or development

__version__ = '%d.%d.%d' % (__major__, __minor__, __release__)

#__author__ = 'Salvador Vargas-Ortega'
#__license__ = 'MIT'
#__author_email__ = 'svortega@gmail.com'
#__maintainer_email__ = 'fem2ufo_users@googlegroups.com'
#__url__ = 'http://fem2ufo.readthedocs.org'
#__downloadUrl__ = "http://bitbucket.org/svortega/fem2ufo/downloads"

#




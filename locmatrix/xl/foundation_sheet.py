# Copyright (c) 2017 LoCmatrix
#
# Python stdlib imports
import re
import sys


# package imports
import xlwings as xw
import locmatrix.model.soil as FEsoil
import locmatrix.model.material as FEmat
import locmatrix.process.units as units
#
#
def get_2D_data(sheet_name, _row):
    """
    """
    _row += 1
    _start = _row
    sht = xw.sheets[sheet_name]
    _col = sht.range(_row, 1).expand('right').value
    _col_no = len(_col)
    _lines = sht.range(_row, _col_no).expand('down').value
    _row = _start + len(_lines) - 1
    datalist = sht.range((_start, 1), (_row, _col_no)).value
    
    return datalist, _row

#
#
def get_section_row(_lines, _row):
    """
    """
    for line in _lines:
        if re.search(r"\bfoundation\s*section\b", str(line), re.IGNORECASE):
            return 'foundation', _row
        
        elif re.search(r"\bsoil\s*layer(s)?\s*section\b", str(line), re.IGNORECASE):
            return 'layers', _row

        elif re.search(r"\bspring(s)?\s*section\b", str(line), re.IGNORECASE):
            return 'spring', _row
        #
        _row += 1
    #
    return False, False
#
#  
#
#
def get_flag_1D_location(_items, flag):
    """
    """
    x = 0
    for line in _items:
        if re.search(flag, line, re.IGNORECASE):
            return  x
        x += 1
    print('  *** warning flag = {:} not found'.format(flag))
    return None
#
def get_curve(sht, _start, _step, _line,
              _force_unit=None, _length_unit=None):
    """
    """
    #if _force_unit and _length_unit:
    #    
    if _force_unit:
        _force_unit = units.find_force_unit(_line[_step])
        _length_unit = 'metre' #  dummy
        _index = 1
        
    else:
        _length_unit = units.find_length_unit(_line[_step])
        _force_unit = 'newton' # dummy
        _index = 0
    #
    _factor = units.convert_units_SI(_length_unit, _force_unit)
    #
    _spring = sht.range(_start, _step+2).expand('right').value
    _spring = [float(_item)*_factor[_index] for _item in _spring]
    return _spring
#
def get_springs(sheet_name, _row, 
                flag_row='Curve Data Points'):
    """
    """
    _start = _row
    _start += 1
    sht = xw.sheets[sheet_name]
    _col = sht.range(_start, 1).expand('right').value
    
    # get spring headers
    _step = get_flag_1D_location(_col, flag_row)
    
    header = {'number': None, 'force' : [], 'displacement' : []}
    datalist = {}
    
    _data, _row = get_2D_data(sheet_name, _row)
    i = 0
    for _line in _data:
        if 'Spring Name' in _line:
            _start += 1
            continue
        elif _line[0]:
            _name = _line[0]
            _number = int(_line[1])
            datalist[_name] = {'number': _number, 'name' : _name,
                               'force' : [], 'displacement' : []}
        #
        if 'force' in _line[_step].lower():
            _force_unit = units.find_force_unit(_line[_step])
            _force = get_curve(sht, _start, _step, _line,
                                _force_unit=_force_unit)
            datalist[_name]['force'] = _force
        elif 'disp' in _line[_step].lower():
            _length_unit = units.find_length_unit(_line[_step])
            _disp = get_curve(sht, _start, _step, _line,
                               _length_unit=_length_unit)
            datalist[_name]['displacement'] = _disp
        #
        _start += 1
    
    return datalist, _row
#
#
#
def get_data(sheet_name, _row=1):
    """
    """
    # initialize search in cell A1
    _section = True
    
    while _section:
        _search = xw.sheets[sheet_name].range(_row, 1).expand('down').value
        if not _search:
            break
        
        _section, _row = get_section_row(_search, _row)
        # step one to avoid separator in section
        _row += 1
        
        if _section == 'foundation':
            _soil, _row = get_2D_data(sheet_name, _row)
            _row += 1
        
        elif _section == 'layers':
            _layers, _row = get_2D_data(sheet_name, _row)
            _row += 1
        
        elif _section == 'spring':
            _springs, _row = get_springs(sheet_name, _row)
            _row += 1
        
        else:
            print('  *** error Foundation section not found')
            sys.exit()
    
    return _soil, _layers, _springs
#
#
def get_layer_object(_layers, _springs):
    """
    """
    _factor_length = 1.0 # default metre
    layers = {}
    _depth_ramainder = 0
    for _layer in _layers:
        if 'Layer Name' in _layer[0]:
            _factor_length = units.convert_units_length_SI(_layer[2])
            continue
        _name = _layer[0]
        _nolayer = int(_layer[1])
        _depth = float(_layer[2]) * _factor_length
        _sublayers = int(_layer[3])
        
        layers[_nolayer] = FEsoil.Layer(_name, _nolayer)
        layers[_nolayer].depth = _depth
        layers[_nolayer].sublayers = _sublayers
        layers[_nolayer].thickness = _depth - _depth_ramainder
        _depth_ramainder = _depth
        #
        py = _springs[_layer[4]]
        if not 'PY_' in py['name']:
            py['name'] = 'PY_' + py['name']
        layers[_nolayer].curve['PY'] = FEsoil.SpringElement(_layer[4], py['number'])
        layers[_nolayer].curve['PY'].force = py['force']
        layers[_nolayer].curve['PY'].displacement = py['displacement']
        #
        tz = _springs[_layer[5]]
        if not 'TZ_' in tz['name']:
            tz['name'] = 'TZ_' + tz['name']
        layers[_nolayer].curve['TZ'] = FEsoil.SpringElement(_layer[5], tz['number'])
        layers[_nolayer].curve['TZ'].force = tz['force']
        layers[_nolayer].curve['TZ'].displacement = tz['displacement']
        #
        qz = _springs[_layer[6]]
        if not 'QZ_' in qz['name']:
            qz['name'] = 'QZ_' + qz['name']
        layers[_nolayer].curve['QZ'] = FEsoil.SpringElement(_layer[6], qz['number'])
        layers[_nolayer].curve['QZ'].force = qz['force']
        layers[_nolayer].curve['QZ'].displacement = qz['displacement']        
        #
    #
    return layers
#
def get_soil_object(_soil, layers):
    """
    """
    soil = {}
    
    for x in range(len(_soil[0])):
        if 'name' in _soil[0][x].lower():
            _location_name = _soil[1][x]
        
        elif 'number' in _soil[0][x].lower():
            _elev_number = int(_soil[1][x])
        
        elif 'diameter' in _soil[0][x].lower():
            _factor_length = units.convert_units_length_SI(_soil[0][x])
            _pile_diameter = float(_soil[1][x]) * _factor_length
        
        elif 'mudline' in _soil[0][x].lower():
            _factor_length = units.convert_units_length_SI(_soil[0][x])
            _mudline_elev = float(_soil[1][x]) * _factor_length
    #
    soil[_elev_number] = FEsoil.Soil(_location_name,
                                       _elev_number)
    
    soil[_elev_number].mudline = _mudline_elev
    soil[_elev_number].diameter = _pile_diameter
    soil[_elev_number].layers = layers
    
    return soil
#
def get_spring_object(_springs):
    """
    """
    _mat = {}
    for _key, _spr in _springs.items():
        _no = _spr['number']
        _name = _spr['name']
        _mat[_name] = FEmat.Material(_key, _no)
        _mat[_name].spring = _spr
        _mat[_name].type = 'spring'
    #
    return _mat
#
#
def get_foundation_data(sheet_name, _row=1):
    """
    """
    _soil, _layers, _springs = get_data(sheet_name, _row=1)
    layers = get_layer_object(_layers, _springs)
    _material = get_spring_object(_springs)
    soil = get_soil_object(_soil, layers)
    #
    return soil, _material
#
#

    
    
# Copyright (c) 2017 locmatrix
#
# Python stdlib imports
#import re
#import sys



# package imports
import xlwings as xw

#
#
#
#
def read_master(sheet_name, 
                master_row=9,
                condition_row=14,
                control_row=16):
    """
    """
    #_number = row_number
    # Get sheet names
    sht = xw.sheets[sheet_name]
    _factored = sht.range(master_row, 1).value
    _actual = sht.range(master_row, 2).value
    _meto =  sht.range(master_row, 3).value
    _soil = sht.range(master_row, 4).value
    
    # get condition data
    _comb_name = sht.range(condition_row, 1).value
    _analysis = sht.range(condition_row, 1).value
    _meto_name = sht.range(condition_row, 3).value
    _soil_name =  sht.range(condition_row , 4).value    
    
    # get control data
    _format = sht.range(control_row, 9).value
    _design = sht.range(control_row, 10).value
    _print = sht.range(control_row, 12).expand('down').value
    
    # overwrite data
    _water_depth = sht.range(29, 4).value
    _mudline = sht.range(40, 4).value
    #_surface = _mudline + _water_depth
    
    #
    master = {'format': _format, 
              'design_condition' : [_design, _comb_name], 
              'analysis' : [_analysis, _meto_name, _soil_name],
              'factored' : [_factored, _print[0]],
              'actual': [_actual, _print[1]], 
              'metocean' : [_meto, _print[2]],
              'foundation' : [_soil, _print[3]],
              'water_depth' : _water_depth,
              'mudline' : _mudline}
    
    return master
#
#
def read_health_checks(sheet_name, 
                       master_row=9):
    """
    """
    # Get sheet names
    sht = xw.sheets[sheet_name]
    
    _soil = sht.range(master_row, 2).value
    _curve = sht.range(master_row, 3).value
    
    master = {'foundation' : [_soil, _curve]}
    
    return master
#
#
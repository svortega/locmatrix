#
# Copyright (c) 2017 LoCmatrix
#

# Python stdlib imports
import copy
import re
import sys

# package imports
import xlwings as xw
import locmatrix.model.metocean as FEmet
import locmatrix.model.analysis as FEcomb
import locmatrix.model.load as FEload
import locmatrix.process.operations as operations
import locmatrix.process.units as units
#
#
#
#
def get_design_load(lineIn):
    """
    """
    _key = {"MaxBaseShear": r"\b(max(imum)?\s*b(ase)?\s*s(hear)?)\b",
            "MaxOMoment": r"\b(max(imum)?\s*o(verturning|t)?\s*m(oment)?)\b",
            "MinBaseShear": r"\b(min(imum)?\s*b(ase)?\s*s(hear)?)\b",
            "MinOMoment": r"\b(min(imum)?\s*o(verturning|t)?\s*m(oment)?)\b",
            "BothLoads": r"\b(both\s*(loads)?)\b",
            "MinBothLoads": r"\b(min(imum)?\s*both\s*(loads)?)\b",
            "NoDesignLoads": r"\b(no\s*design\s*loads)\b"}
    keyWord, lineOut, _match = operations.select(lineIn, _key)
    #
    return keyWord
#
#
def get_wave_theory(lineIn):
    """
    """
    _key = {"Stokes5": r"\b(stoke(s)?((\s*|\_)5((\s')?th)?|fifth\s*(order)?)?)\b",
            "Airy": r"\b(airy)\b",
            "Cnoidal": r"\b(cnoidal)\b",
            "CalmSea": r"\b(calm(\s*|\_)sea)\b",
            "StreamFunction": r"\b(stream(\s*function)?(\s*theory)?)\b",
            "user_defined": r"\b(user(\s*defined)?)\b"}
    keyWord, lineOut, _match = operations.select(lineIn, _key)
    #
    return keyWord
#
#
def get_wind_theory(lineIn):
    """
    """
    _key = {"general": r"\b(general)\b",
            "normal": r"\b(normal)\b",
            "abs": r"\b(abs)\b",
            "extreme": r"\b((extreme(\s*(\_)?api)?|api))\b",
            "extreme_api21": r"\b((extreme\s*(\_)?)?api\s*(\_)?21)\b"}
    keyWord, lineOut, _match = operations.select(lineIn, _key)
    return keyWord
#
#
def get_section_row(_lines, _row):
    """
    """
    #
    for line in _lines:
        if re.search(r"Metocean\s*section", str(line), re.IGNORECASE):
            return 'metocean', _row
        
        elif re.search(r"combination\s*section", str(line), re.IGNORECASE):
            return 'combination', _row        
        
        elif re.search(r"wave\s*section", str(line), re.IGNORECASE):
            return 'wave', _row
        
        elif re.search(r"current\s*section", str(line), re.IGNORECASE):
            return 'current', _row
        
        elif re.search(r"wind\s*section", str(line), re.IGNORECASE):
            return 'wind', _row
        
        elif re.search(r"wind\s*area(s)?\s*section", str(line), re.IGNORECASE):
            return 'area', _row
        
        elif re.search(r"wind\s*area(s)?\s*group(s)?\s*section", str(line), re.IGNORECASE):
            return 'area_combination', _row         
        #
        _row += 1
    #
    return False, False
#
def get_wave_set_row(sheet_name, _lines):
    """
    """
    x = 0
    for line in _lines:
        x += 1
        if re.search(r"group|set\s*name|id", str(line), re.IGNORECASE):
            return  x
    #
    #print('  *** warning wave set not found')
    return False
#
def get_seastate_row(sheet_name, _lines):
    """
    """
    x = 0
    for line in _lines:
        x += 1
        if re.search(r"items(\s*name|id)?", str(line), re.IGNORECASE):
            return  x
    #
    #print('  *** warning sea state not found')
    return False
#
#
def get_flag_1D_location(_items, flag):
    """
    """
    x = 0
    for line in _items:
        if re.search(flag, line, re.IGNORECASE):
            return  x
        x += 1
    print('  *** warning flag = {:} not found'.format(flag))
    return None
#
def get_2D_data(sheet_name, _row, flag_row, flag_col):
    """
    """
    #
    _start = _row
    sht = xw.sheets[sheet_name]
    _lines = sht.range(_row, 1).expand('down').value
    # get wave headers
    _step = get_flag_1D_location(_lines, flag_row)
    
    # check if empty cells
    try:
        _row += _step
    except TypeError:
        return None, _row
    
    _headers = sht.range(_row, 1).expand('right').value
    #
    # get wave combination column
    _col = get_flag_1D_location(_headers, flag_col) + 1
    # get lengh of the data
    _vertical = sht.range(_row, _col).expand('down').value
    if isinstance(_vertical, str):
        return None, _row
    
    total_rows_2 = len(_vertical) - 1
    #
    #total_rows = max(total_rows_1, total_rows_2)
    total_rows = total_rows_2
    # get all wave data
    _row = _row + total_rows
    datalist = sht.range((_start, 1), 
                        (_row, len(_headers))).value
    #
    return datalist, _row
#
#
def get_2D_data_current(sheet_name, _row, flag_row, flag_col):
    """
    """
    #
    _start = _row
    sht = xw.sheets[sheet_name]
    _lines = sht.range(_row, 1).expand('down').value
    # get wave headers
    _step = get_flag_1D_location(_lines, flag_row)
    try:
        _row += _step
    except TypeError:
        return None, _row
    #
    _wave_headers = sht.range(_row, 1).expand('right').value
    #
    # get wave combination column
    _col = get_flag_1D_location(_wave_headers, flag_col) + 1
    # get lengh of the data
    _wave_vertical = sht.range(_row, _col).expand('down').value
    total_rows_2 = len(_wave_vertical) - 1
    #
    #total_rows = max(total_rows_1, total_rows_2)
    total_rows = total_rows_2
    # get all wave data
    _row = _row + total_rows
    datalist = sht.range((_start, 1), 
                        (_row, len(_wave_headers))).value
    #   
    #
    return datalist, _row
#
def get_2D_data_meto(sheet_name, _row):
    """
    """
    _row += 1
    _start = _row
    sht = xw.sheets[sheet_name]
    _col = sht.range(_row, 1).expand('right').value
    _col_no = len(_col)
    
    _lines = sht.range(_row, _col_no).expand('down').value
    
    _row = _start + len(_lines) - 1
    datalist = sht.range((_start, 1), 
                        (_row, _col_no)).value
    
    return datalist, _row
#
def get_metoceam_name(sheet_name, _row):
    """
    """
    seastate, _row = get_2D_data_meto(sheet_name, _row)
    
    return seastate, _row
#
def get_combination_data(sheet_name, _row):
    """
    """
    _lines = xw.sheets[sheet_name].range(_row, 1).expand('down').value
    # get seastate
    _start  = get_seastate_row(sheet_name, _lines)
    _start += _row     
    #
    datalist, _row = get_2D_data(sheet_name, _start, 
                                 flag_row='combination\s*name', 
                                 flag_col='number')
    #
    return datalist, _row
#
def get_wave_data(sheet_name, _row):
    """
    """
    #
    _row += 1
    datalist, _row = get_2D_data(sheet_name, _row, 
                                 flag_row='wave\s*name', 
                                 flag_col='theory')
    #
    return datalist, _row
#
def get_current_data(sheet_name, _row):
    """
    """
    _row += 1
    datalist, _row = get_2D_data_current(sheet_name, _row, 
                                         flag_row='current\s*name', 
                                         flag_col='elevation')
    
    return datalist, _row
#
def get_wind_data(sheet_name, _row):
    """
    """
    _row += 1
    datalist, _row = get_2D_data(sheet_name, _row, 
                                 flag_row='wind\s*name', 
                                 flag_col='velocity')
    
    return datalist, _row
#
def get_areas_data(sheet_name, _row):
    """
    """
    _row += 1
    datalist, _row = get_2D_data(sheet_name, _row, 
                                 flag_row='area\s*name', 
                                 flag_col='drag')
    
    return datalist, _row
#
def get_area_combination(sheet_name, _row):
    """
    """
    _row += 1
    datalist, _row = get_2D_data(sheet_name, _row,
                                 flag_row='group\s*name', 
                                 flag_col='number')
    
    return datalist, _row    
#
#
def get_data(sheet_name):
    """
    """
    #
    _wave = {}
    _current = []
    _wind = []
    _area = []
    _area_comb = []
    #
    # initialize search in cell A1
    _row = 1
    _section = True
    
    while _section:
        _search = xw.sheets[sheet_name].range(_row, 1).expand('down').value
        if not _search:
            break
        _section, _row = get_section_row(_search, _row)
        # step one to avoid separator in section
        _row += 1
        
        if _section == 'metocean':
            _seastate, _row = get_metoceam_name(sheet_name, _row)
            _row += 1
        
        elif _section == 'combination':
            _comb, _row = get_combination_data(sheet_name, _row)
            _row += 1
        
        elif _section == 'wave':
            _wave, _row = get_wave_data(sheet_name, _row)
            _row += 1
        
        elif _section == 'current':
            _current, _row = get_current_data(sheet_name, _row)
            _row += 1
        
        elif _section == 'wind':
            _wind, _row = get_wind_data(sheet_name, _row)
            _row += 1
        
        elif _section == 'area':
            _area, _row = get_areas_data(sheet_name, _row)
            _row += 1
        
        elif _section == 'area_combination':
            _area_comb, _row = get_area_combination(sheet_name, _row)
            _row += 1         
        else:
            print('  *** error Section not found')
            sys.exit()
    #
    return _seastate, _comb, _wave, _current, _wind, _area, _area_comb
#
#
def get_wave_object(data):
    """
    """
    #
    _factor_height = 1.0  # metre default
    _factor_water_depth = 1.0 # metre default
    #
    _wave = {}
    for _item in data:
        if re.search(r"wave\s*name", _item[0], re.IGNORECASE):
            _factor_height = units.convert_units_length_SI(_item[2])
            _factor_water_depth = units.convert_units_length_SI(_item[4])
            continue
        # wave data
        name = _item[0]
        number = int(_item[1])
        _wave[name] = FEmet.Wave(number, name)
        _wave[name].theory = get_wave_theory(_item[5])
        try:
            _wave[name].height = float(_item[2]) * _factor_height
        except TypeError:
            _wave[name].height = None
        _wave[name].period = _item[3]
        _wave[name].water_depth = float(_item[4]) * _factor_water_depth
        
        if 'stream' in _wave[name].theory.lower():
            _wave[name].order = _item[6]
        
        if _item[7]:
            _wave[name].phase = _item[7]
    #
    return _wave
#
def get_current_object(data):
    """
    """
    _factor_elevation = 1.0  # metre default
    _factor_velocity = 1.0 # metre/second default
    _current = {}
    for _item in data:
        # current data
        if _item[0]:
            if re.search(r"current\s*name", _item[0], re.IGNORECASE):
                _factor_elevation = units.convert_units_length_SI(_item[2])
                _factor_velocity = units.convert_units_velocity_SI(_item[3])
                continue
            else:
                name = _item[0]
                number = int(_item[1])
                _current[name] = FEmet.Current(number, name)
        #
        _current[name].profile.append([float(_item[2]) * _factor_elevation, 
                                       float(_item[3]) * _factor_velocity])
    #
    return _current
#
def get_wind_object(data):
    """
    """
    _factor_height = 1.0  # metre default
    _factor_velocity = 1.0 # metre/second default
    _wind = {}
    for _item in data:
        if re.search(r"wind\s*name", _item[0], re.IGNORECASE):
            _factor_velocity = units.convert_units_velocity_SI(_item[2])
            _factor_height = units.convert_units_length_SI(_item[3])   
            continue
        # wind data
        name = _item[0]
        number = int(_item[1])
        formula = _item[4]
        _wind[name] = FEmet.Wind(number, name)
        _wind[name].formula = get_wind_theory(formula)
        _wind[name].velocity = float(_item[2]) * _factor_velocity
        _wind[name].height = float(_item[3]) * _factor_height   
        
        if formula == 'general':
            try:
                _wind[name].power = float(_item[6])
            except TypeError:
                _wind[name].power = None
            
            try:
                _wind[name].gust_factor = float(_item[7])
            except TypeError:
                _wind[name].gust_factor = 1.0
        
        if formula == 'abc':
            try:
                _wind[name].power = float(_item[6])
            except TypeError:
                _wind[name].power = None
            
            try:
                _wind[name].period_ratio = float(_item[5])
            except TypeError:
                _wind[name].period_ratio = 1.0
            
        else:
            #if formula == 'extreme_api21':
            #    try:
            #        _wind[name].duration = float(_item[8])
            #    except TypeError:
            #        _wind[name].duration = 1.0
            #    except IndexError:
            #        _wind[name].duration = 1.0                
            #
            try:
                _wind[name].period_ratio = float(_item[5])
            except TypeError:
                _wind[name].period_ratio = 1.0
        #
    #
    return _wind
#
def get_wind_area_object(data):
    """
    """
    _factor_length_x = 1.0
    _factor_length_y = 1.0
    _factor_length_z = 1.0
    _areas = {}
    for _item in data:
        if re.search(r"area\s*name", _item[0], re.IGNORECASE):
            _factor_area = (units.convert_units_area_SI(_item[2]))**2
            _factor_length_x = units.convert_units_length_SI(_item[4])
            _factor_length_y = units.convert_units_length_SI(_item[5])
            _factor_length_z = units.convert_units_length_SI(_item[6]) 
            continue
        # wind area data
        name = _item[0]
        number = int(_item[1])
        _areas[name] = FEload.Equipment(name)
        _areas[name].number = number
        _areas[name].type = 'wind_area'
        # get area dimensions
        _total_area = float(_item[2]) * _factor_area
        _size = round(_total_area**0.50, 2)
        _areas[name].size = _size, _size, 0.10
        # get Centroid
        _coord = [float(_item[4]) * _factor_length_x, 
                  float(_item[5]) * _factor_length_y, 
                  float(_item[6]) * _factor_length_z]
        _areas[name].mass = 0, _coord
        # get proyection:
        _shape_factor = float(_item[3])
        if 'x' in _item[7].lower():
            _areas[name].wind_pressure_x(_shape_factor, 0)
        elif 'y' in _item[7].lower():
            _areas[name].wind_pressure_y(_shape_factor, 0)
        else:
            _areas[name].wind_pressure_z(_shape_factor, 0)
        # get interface joints
        _areas[name].joints.extend(_item[8:])
    #
    return _areas
#
def update_wind_areas(areas, data):
    """
    """
    _wind_comb = {}
    for _item in data:
        if re.search(r"group\s*name", _item[0], re.IGNORECASE):
            _item
            continue
        # wind combination data
        name = _item[0]
        number = int(_item[1])
        _wind_comb[name] = []
        for area_name in _item[2:]:
            try:
                _area = areas[area_name]
                _area.load_interface.append(name)
                _wind_comb[name].append(_area.name)
            except KeyError:
                continue
    
    return _wind_comb
#
#
def get_metocean_data(sheet_name):
    """
    """
    # get sestate data
    (_seastates, _comb_data, _wave_data, 
     _curr_data, _wind_data, _area_data,
     _area_comb) = get_data(sheet_name)
    # get wave
    wave = get_wave_object(_wave_data)
    # get current
    if _curr_data:
        current = get_current_object(_curr_data)
    else:
        current = {}   
    # get wind
    if _wind_data:
        wind = get_wind_object(_wind_data)
    else:
        wind = {}
    # get wind areas
    if _area_data:
        area = get_wind_area_object(_area_data)
        area_comb = update_wind_areas(area, _area_comb)
    else:
        area = {}
    #
    _water_depth = []
    _wave_no = []
    for _wave in wave.values():
        _wave_no.append(_wave.number)
        _water_depth.append(float(_wave.water_depth))
    #
    _water_depth = list(set(_water_depth))
    _wave_no = max(_wave_no)
    #
    for  x in range(len(_seastates[0])):
        if 'name' in _seastates[0][x].lower() :
            _set_name = _seastates[1][x]
        
        elif 'number' in _seastates[0][x].lower() :
            _set_number = int(_seastates[1][x])
    #
    _metocean = {}
    for _item in _comb_data:
        if 'combination name' in _item[0].lower():
            continue
        #
        # wave data
        name = _item[0]
        number = int(_item[1])
        _metocean[name] = FEmet.MetoceanCombination(number, name)
        try:
            _metocean[name].design_load = get_design_load(_item[2])
        except TypeError:
            _metocean[name].design_load = "NoDesignLoads"
        #
        # check wave
        if _item[3]:
            _metocean[name].wave = copy.deepcopy(wave[_item[3]])
            _metocean[name].wave_direction = _item[4]
            _metocean[name].wave_kinematics = _item[5]
        else:
            try:
                _metocean[name].wave = wave['calmsea']
            except KeyError:
                _wave_no += 1
                wave['calmsea'] = FEmet.Wave(_wave_no, 'calmsea')
                wave['calmsea'].theory = 'calm sea' 
                wave['calmsea'].water_depth = min(_water_depth)
                _metocean[name].wave = wave['calmsea']
        #
        # check buoyancy
        if _item[6]:
            _metocean[name].buoyancy = _item[6]
        else:
            _metocean[name].buoyancy = 'off'
        #
        # check current
        if _item[7]:
            _metocean[name].current = copy.deepcopy(current[_item[7]])
            _metocean[name].current_blockage = _item[8]
            _metocean[name].current_stretching = _item[9]
            _metocean[name].current_direction = _item[10]
        #
        # check wind
        if _item[11]:
            _metocean[name].wind = copy.deepcopy(wind[_item[11]])
            _metocean[name].wind_direction = _item[12]
            try:
                for _comb_name in _item[13:]:
                    _areas = area_comb[_comb_name]
                    _metocean[name].wind.name = _comb_name
                    for _area_name in _areas:
                        try:
                            _metocean[name].wind_areas.append(area[_area_name])
                        except KeyError:
                            continue
            except KeyError:
                pass
    #
    condition = FEmet.Condition(1, _set_name)
    condition.water_depth = _water_depth    
    #
    _case = {}
    _case[_set_name] = FEcomb.Case(_set_number, _set_name)
    _case[_set_name].metocean_combination = _metocean
    _case[_set_name].condition = condition
    #
    #print('ok')
    return _case
#
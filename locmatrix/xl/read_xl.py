# Copyright (c) 2017 locmatrix
#
# Python stdlib imports
#import math

# package imports
import locmatrix.xl.metocean_sheet as xlmetoc
import locmatrix.xl.comb_sheet as xlcomb
import locmatrix.xl.master_sheet as xlmaster
import locmatrix.xl.foundation_sheet as xlsoil
#
#
#
def get_factored_combinations(sheet_name, design_condition):
    """
    """
    #
    #
    combination, functional = xlcomb.read_factored_combinations(sheet_name,
                                                                design_condition)
    #
    return combination, functional
# 
#
def get_basic_combinations(sheet_name, metocean,
                           design_condition):
    """
    """
    #
    #
    combination, functional = xlcomb.read_basic_combinations(sheet_name, metocean,
                                                             design_condition)
    #
    #print('ok')
    return combination, functional
#
#
def get_metocean(sheet_name):
    """
    """
    _case = xlmetoc.get_metocean_data(sheet_name)
    
    return _case
#
#
def get_master(sheet_name):
    """
    """
    master = xlmaster.read_master(sheet_name)
    
    return master
#
#
def get_foundation(sheet_name):
    """
    """
    _soil = xlsoil.get_foundation_data(sheet_name)
    #print('--->')
    return _soil
#
def get_health_checks(sheet_name):
    """
    """
    _checks = xlmaster.read_health_checks(sheet_name)
    
    return _checks
#
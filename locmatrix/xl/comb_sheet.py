
#
# Python stdlib imports
#import re
#import sys



# package imports
import xlwings as xw
import locmatrix.model.load as FEload

#
#
#
#
#
def read_workbook(sheet_name):
    """
    """
    #
    sht = xw.sheets[sheet_name]
    _lcc = sht.range(2, 5).expand('right').value
    # check if only one lc case
    if not isinstance(_lcc, list):
        _lcc = [_lcc]
    
    _lcc2 = sht.range((3, 1), (3, len(_lcc) + 4)).value
    _blc = sht.range(4, 1).expand('down').value
    #
    _list = []
    for x in range(len(_blc)):
        _list.append(sht.range((4 + x, 1), (4 + x, len(_lcc) + 4)).value)
    #
    _start = [None, None, None, None]
    _start.extend(_lcc)
    #
    _list.insert(0, _lcc2)
    _list.insert(0, _start)
    #
    return _list
#
#
def get_load_name(lcomb):
    """
    """
    _total = len(lcomb)
    _basic = {}
    for x in range(2, _total):
        _name = lcomb[x][0]
        _basic[_name] = x
    #
    #
    _total = len(lcomb[0])
    _comb = {}
    for x in range(4, _total):
        _name = lcomb[0][x]
        _comb[_name] = x
    #
    return _basic, _comb
#
#
def read_factored_combinations(sheet_name, design_condition):
    """
    """
    #
    lcomb = read_workbook(sheet_name)
    _basic, _comb = get_load_name(lcomb)
    #
    combination = {}
    functional = {}
    #
    for comb_name, _index2 in sorted(_comb.items()):
        try:
            comb_number = int(lcomb[1][_index2])
        except TypeError:
            comb_number = None        
        #
        combination[comb_name] = FEload.LoadCombination(comb_number, 
                                                        comb_name, 1)
        combination[comb_name].design_condition = design_condition
        
        basic_comb = []
        for basic_name, _index in sorted(_basic.items()):
            basic_number = lcomb[_index][1]
            lc_type = lcomb[_index][2]            
            #
            try:
                factor = float(lcomb[_index][_index2])
                basic_comb.append([basic_name, factor])
                functional[basic_name] = FEload.LoadCombination(basic_number, 
                                                                basic_name, 0)
            except TypeError:
                continue
            #
        #
        combination[comb_name].functional_load = basic_comb
    #
    #
    return combination, functional
# 
#
def read_basic_combinations(sheet_name, metocean,
                            design_condition):
    """
    """
    #
    lcomb = read_workbook(sheet_name)
    _basic, _comb = get_load_name(lcomb)
    #
    combination = {}
    functional = {}
    #
    for comb_name, _index2 in sorted(_comb.items()):
        try:
            comb_number = int(lcomb[1][_index2])
        except TypeError:
            comb_number = None
        #
        combination[comb_name] = FEload.LoadCombination(comb_number, 
                                                        comb_name, 0)
        combination[comb_name].design_condition = design_condition
        #
        basic_load = []
        wave = []
        for basic_name, _index in _basic.items():
            try:
                basic_number = int(lcomb[_index][1])
            except TypeError:
                basic_number = None
            
            lc_type = lcomb[_index][2]
            if not lc_type:
                lc_type = 'basic load'
            #
            try:
                factor = float(lcomb[_index][_index2])
                #
                if 'metocean' in lc_type:
                    _seastate  = metocean[basic_name]
                    wave.append([_seastate, factor])
                else:
                    try:
                        functional[basic_name] 
                    except KeyError:
                        functional[basic_name] = FEload.LoadCombination(basic_number, 
                                                                        basic_name)
                    
                    basic_load.append([basic_name, factor])
                    
            except TypeError:
                continue
        #
        if basic_load:
            combination[comb_name].functional_load = basic_load
        if wave:
            combination[comb_name].metocean_load = wave
    #    
    #
    #print('ok')
    return combination, functional
#